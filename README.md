Ansaldo Vipro
==============

MicroLok V2

Prerequisite
------------

1. Any Linux based OS (Ubuntu, Debian etc.)
2. git
3. nodejs v7.6.0
4. npm 4.1.2

Installation
-------------
1. After checking out the code from git cd to web-server/web-server
2. Switch branch to json_data_changes

	`git checkout json_data_changes`

3. Install node modules.

	`npm install`

Running Microlok Application
-----------------------------
from the current director (... /web-server/web-server) run following command.

	`node server.js`