var express = require('express');
var session = require('express-session');
var authenticate = require('./../lib/login').authenticate;
var router = express.Router();
var util = require('util');
var FileStore = require('session-file-store')(session);
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var config = require('./../config');
var path = require('path');
var rootDirectory = path.resolve('.');
var Q = require('q');
var loggedinSession;
var routerList = [];

localSessionList = [];
var filestore_sessions_list = [];
var sess_options = {
    path: path.join(require('os').tmpdir(), 'sessions'),
    reapAsync : false,
    logFn : function(){},
    reapInterval : 10,
    ttl : 60 * config.CONFIG_SESSION_TIME_OUT
}

// -------------- Function to sync localSessionList with file store sessions. ---------------------
function getFileStoreSessionList(callback){
	var deferred = Q.defer();
      file_store.list(function (a, session_list) {
	    filestore_sessions_list = session_list;
      callback(filestore_sessions_list);
      deferred.resolve("resolve");
	});
	return deferred.promise;
}
function refresh_localSessionList(session_list){
	for (var i=0; i < localSessionList.length; i++){
		if(session_list.indexOf(localSessionList[i] +'.json') < 0){
		   localSessionList.pop(localSessionList[i]);
       i--;
		}
	}
}
// -------------- END of Function to sync localSessionList with file store sessions. ---------------

var file_store = new FileStore(sess_options);

  var sessionMiddleware = session({
  	store : file_store,
  	resave : false,
  	saveUninitialized : false,
  	secret : 'asts123',
    rolling : true,
    cookie : {	httpOnly: false,
  	secure : false,
  	}
  });

router.use(sessionMiddleware);

function getAllRouteList(){
  var list = [];
  router.stack.forEach(function(Layer){
    if(Layer.route)
    list.push(Layer.route.path);
  });
  return list;
}

router.all('*', function(req, res, next){
  if (req.secure) {
      return next();
  };
  res.redirect('https://'+req.hostname+':'+ config.HTTPS_PORT +req.url );
});

    router.use(function(req, res, next) {
  if(req.session){
    req.session.cookie.expires = false;
    req.session.cookie.maxAge = 1000 * 60 * config.CONFIG_SESSION_TIME_OUT + 10000; // + 10 seconds
	if (typeof req.session.user_mode === 'undefined') {
		req.session.user_mode = 'view_mode';
		res.app.locals.page_nav = 1;
		req.session.vopstatus = 'CPS Up';
		req.session.enableSessionTimeout = 'false';
	}
	else {
            res.app.locals.page_nav = req.session.user_mode == 'view_mode' ? '1' : '2';
  }
  res.app.locals.enableSessionTimeout = req.session.enableSessionTimeout;

	// ----------------- Check for session limit ----------------------------
	getFileStoreSessionList(refresh_localSessionList).then(function(){
    if(loggedinSession && localSessionList.indexOf(loggedinSession) < 0){
      res.app.locals.edit_mode = '0';
      res.app.locals.enableSessionTimeout;
      delete res.app.locals.user;
      loggedinSession = undefined;
    }
		if (localSessionList.length >= parseInt(config.MAX_SESSION_COUNT)){
		   if (localSessionList.indexOf(req.session.id) > -1){
			      res.app.locals.page_nav = req.session.user_mode == 'view_mode' ? '1' : '2';
		    }
		   else {
    			res.app.locals.page_nav = 9;
          if(routerList.indexOf(req.url)> 0) {
            res.sendFile(rootDirectory +'/views/block.html');
            return;
          }
		    }
		}
		else if(localSessionList.length < parseInt(config.MAX_SESSION_COUNT)){
			       res.app.locals.page_nav = req.session.user_mode == 'view_mode' ? '1' : '2';
		        if(localSessionList.indexOf(req.session.id) < 0){
				localSessionList.push(req.session.id);
			}
		}
        next();
	});
  // ----------------- END of Check for session limit ----------------------

	}
    });

    router.get('/', function (req, res) {
      res.app.locals.page_load = '1';
      res.render('index');
    });

    router.get('/exit-config', function (req, res) {
      res.app.locals.page_nav = '1';
      req.session.user_mode = 'view_mode';
      req.session.vopstatus = 'CPS Up';
      res.render('index');
    });

    router.post('/look-accept', urlencodedParser, function(req, res){
      if (!req.body) return res.sendStatus(400)
        var status = '0';
        var result = '1';
        res.app.locals.page_nav = '2';
        req.session.enableSessionTimeout = 'true';
        res.app.locals.enableSessionTimeout = req.session.enableSessionTimeout;
        req.session.vopstatus = 'Configuration';
        var data = { 'status_code': status, 'result': result  };
        res.send(data);
    });

    router.post('/wait-confirm', urlencodedParser, function(req, res){
      if (!req.body) return res.sendStatus(400)
        var status = '0';
        req.session.vopstatus = 'Reset Wait';
        var data = { 'status_code': status };
        res.send(data);
    });

    router.post('/login', urlencodedParser, function(req, res){
      var status = '';
      if (!req.body) return res.sendStatus(400)
      if(req.body.mode == 'view') {
        status = '100';
        req.session.user_mode = "view_config";
        res.app.locals.page_nav = '2';
        req.session.enableSessionTimeout = 'false';
        res.app.locals.enableSessionTimeout = req.session.enableSessionTimeout;
        //res.app.locals.edit_mode = '0';
        var data = { 'status_code': status };
        res.send(data);
        } else {
        authenticate(req.body.password , function(err, user){
        if (user) {
          if (typeof res.app.locals.user === 'undefined'){
							// NEW USER LOGIN -- NEW SESSION
              //req.session.cookie.maxAge = config.CONFIG_SESSION_TIME_OUT;
              //req.session.user = user;
              res.app.locals.user = user;
              req.session.user_mode = "edit_config";
              res.app.locals.edit_mode = '1';
              loggedinSession = req.session.id;
              status = '0';
              var data = { 'status_code': status };
              res.send(data);
             }
            else {
               // USER ALREADY LOGGED IN -- SESSION ACTIVE
               status = '2';
               var data = { 'status_code' : status };
               res.send(data);
             }
          } else {
	           // PASSWORD ERROR
           status = '3';
           var data = { 'status_code': status };
           res.send(data);
            }
          });
        }
      });
/* system-info AJAX call needs to Go  As these values are now sent on socket but can not remove it as sysinfo file is still using it*/
	 router.get('/system-info',function (req, res) {
		 var data = { 'SWVer': '0.01',
							  	'vopstatus': req.session.vopstatus,
									'appname': 'DigiSafe_Integration_Test'
								};
		 res.send(data);
	 });

  router.get('/reset-mlk',function (req, res) {
    var reset_id = '0';
    var cps_status = '';
    var data = { 'status_code': '0',
                 'reset_id': reset_id ,
                 'cps_status' : cps_status
               };
    res.send(data);
  });

  router.post('/reset-mlk',function (req, res) {
    var data = { 'status_code': '0',
                 'reason': "Successful"
               };
    res.send(data);
  });

  router.get('/apply-cfg',function (req, res) {
    var data = { 'status_code': '0'
               };
    res.send(data);
  });

  router.get('/discard-cfg',function (req, res) {
    var data = { 'status_code': '0'
               };
    res.send(data);
  });

    router.get('/linkinfo', function (req, res) {
      res.render('linkinfo');
    });

    router.get('/sysinfo', function (req, res) {
      res.render('sysinfo');
    });

    router.get('/messagemon', function (req, res) {
      res.render('messagemon');
    });

    router.get('/bitandvars', function (req, res) {
      res.render('bitandvars');
    });

    router.get('/userdatalog', function (req, res) {
      res.render('userdatalog');
    });

    router.get('/eventlog', function (req, res) {
      res.render('eventlog');
    });

    router.get('/errorlog', function (req, res) {
      res.render('errorlog');
    });

    router.get('/logicmon', function (req, res) {
      res.render('logicmon');
    });

    router.get('/systime', function (req, res) {
      res.render('systime');
    });
    router.get('/memorydump', function (req, res) {
      res.render('memorydump');
    });
    router.get('/conf_download', function (req, res) {
      res.render('conf_download');
    });

    router.get('/conf_clear', function (req, res) {
      res.render('conf_clear');
    });

    router.get('/conf_general', function (req, res) {
      res.render('conf_general');
    });

    router.get('/conf_links', function (req, res) {
      res.render('conf_links');
    });

    router.get('/conf_route', function (req, res) {
      res.render('conf_route');
    });

    router.get('/conf_timers', function (req, res) {
      res.render('conf_timers');
    });

    router.get('/conf_userdatalog', function (req, res) {
      res.render('conf_userdatalog');
    });

    router.get('/conf_variables', function (req, res) {
      res.render('conf_variables');
    });

    router.get('/conf_boards', function (req, res) {
      res.render('conf_boards');
    });

    router.get('/conf_upload', function (req, res) {
      res.render('conf_upload');
    });

    router.get('/config', function (req, res) {
      res.render('config');
    });
    router.get('/conf_snmp', function (req, res) {
      res.render('conf_snmp');
    });

    (function(){
      routerList = getAllRouteList();
    })();

module.exports = {
  router : router,
  sessionMiddleware : sessionMiddleware
}
