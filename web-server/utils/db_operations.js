var fs = require("fs");
var file = "./database/microlok_eventlog_database.db";
var exists = fs.existsSync(file);
var Q = require('q');
//console.log(exists);
var sqlite3 = require("sqlite3").verbose();
//var db = new sqlite3.Database(file);
var eventData = {};

function getData(sqlQuery, totalRecordsQuery, callback){
  var db = new sqlite3.Database(file);
  var deferred = Q.defer();
  var queryResult;
  db.all(sqlQuery, function(err, rows){
    queryResult = rows;
    db.all(totalRecordsQuery, function(err, rows){
      callback(queryResult, rows);
      deferred.resolve("resolve");
      db.close();
    });
  });totalRecordsQuery
  return deferred.promise;
}


function parseData(dblist, TotalRecords){
  eventData.queryResult = JSON.stringify(dblist);
  eventData.totalRecords = TotalRecords;
  //console.log(eventData);
};
var _getEventlogs = function (sqlQuery, totalRecordsQuery){
  var deferred = Q.defer();
  getData(sqlQuery, totalRecordsQuery, parseData).then(function(){
    //console.log(eventData);
    deferred.resolve(eventData);
  });
  return deferred.promise;
};

var _getAllData = function(){
  var deferred = Q.defer();
    getData("SELECT id EventID, CASE WHEN CAST(strftime('%H', time) AS INTEGER) = 12 THEN substr(strftime('%m/%d %H:%M:%S.%f', time), 1, 16) || ' PM' WHEN CAST(strftime('%H', time) AS INTEGER) > 12 THEN substr(strftime('%m/%d %H:%M:%S.%f', time), 1, 16) || ' PM' ELSE substr(strftime('%m/%d %H:%M:%S.%f', time), 1, 16) || ' AM' END EventTime, description Event, source Source, primaryid EventCode, CASE WHEN severity = 0 THEN 'information' WHEN severity = 1 THEN 'warning' ELSE 'error' END AS EventType FROM tbl_eventlog ORDER BY time DESC", "select count(*) Total from tbl_eventlog;", parseData).then(function(){
    deferred.resolve(eventData.queryResult);
  });
  return deferred.promise;
}

var _gatAllDataErrorLog = function(){
  var deferred = Q.defer();
    getData("SELECT id EventID, CASE WHEN CAST(strftime('%H', time) AS INTEGER) = 12 THEN substr(strftime('%m/%d %H:%M:%S.%f', time), 1, 16) || ' PM' WHEN CAST(strftime('%H', time) AS INTEGER) > 12 THEN substr(strftime('%m/%d %H:%M:%S.%f', time), 1, 16) || ' PM' ELSE substr(strftime('%m/%d %H:%M:%S.%f', time), 1, 16) || ' AM' END EventTime, description Event, source Source, primaryid EventCode, CASE WHEN severity = 0 THEN 'information' WHEN severity = 1 THEN 'warning' ELSE 'error' END AS EventType FROM tbl_eventlog where severity = 2 ORDER BY time DESC", "select count(*) Total from tbl_eventlog where severity = 2;", parseData).then(function(){
    deferred.resolve(eventData.queryResult);
  });
  return deferred.promise;
}

var _deleteRecords = function(){
  var db = new sqlite3.Database(file);
  var deferred = Q.defer();
  db.run("DELETE FROM tbl_eventlog", function(err){
    if(err){
      console.log('error in delete');
      deferred.resolve("error");
    }
    else{
      db.run("insert into tbl_eventlog values (1001, null, null, null, null, null, null, 1, datetime(CURRENT_TIMESTAMP, 'localtime'), 'All event log deleted by application', 0);", function(err){
        deferred.resolve("Delete successfully");
      });
    }
  });
  return deferred.promise;
}

var operations = {
  getEventlogs : _getEventlogs,
  gatAllData : _getAllData,
  gatAllDataErrorLog : _gatAllDataErrorLog,
  deleteRecords : _deleteRecords
}

module.exports = operations;
