function fileXHR(url, params) {
    if (!Prototype) return;
    if (!url) return;
        
    params = params || {};
    params.method = params.method || 'post';
    params.onSuccess = params.onSuccess ||
    function (transport) {
        return
    };
    params.onFailure = params.onFailure ||
    function (transport) {
        return
    };
    
    params.parameters = params.parameters || null;
    var element; 
        
    if (params.parameters) {
        if (Object.isElement(params.parameters)) {
            element = $(params.parameters);
           
        } else {
            element = new Element('div');
            $(document.body).insert(element);
            element.hide();
            for (var key in params.parameters) {
                domElement = new Element('input', {
                    type: 'hidden',
                    name: key,
                    id: '_fid_' + key,
                    value: params.parameters[key]
                });
                element.insert(domElement);
            }
        }
    }

        
    var fname = 'f' + (Math.random() * 1000);
    var iframe = new Element('iframe', {
        'src': 'about:blank',
        'id': fname,
        'name': fname
    });
    $(document.body).insert(iframe);
    iframe.hide();
    

 
    var form = new Element('form', {
        'action': url,
        'enctype': 'multipart/form-data',
        'encoding': 'multipart/form-data',
        'method': params.method,
        'target': iframe.id
    });
    element.wrap(form);
    

     
    iframe.observe('load', function () {
        var doc = iframe.contentDocument ? iframe.contentDocument : frames[fname].document;
        if (!doc || !doc.body || !doc.body.innerHTML) {
            params.onFailure({
                responseText: 'Error processing request'
            });
            return
        }
        if (doc.body.innerHTML) {
            var el = doc.body.getElementsByTagName('pre')[0];
            if (el) {
                params.onSuccess({
                    responseText: el.innerHTML
                });
            } else {
                params.onFailure({
                    responseText: 'Error processing request'
                });
            }
        }
        (function () {
            iframe.remove();
            delete iframe;
            if (params.parameters && Object.isElement(params.parameters)) {
                form.insert({
                    before: params.parameters
                });
            }
            form.remove();
            delete form;
        }).defer();
    });
    

    form.submit();
}
