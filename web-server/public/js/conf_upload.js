/* Setup actions for submit buttons */
(function() {
  var socket = io();
  var IsUploadRunning = false;
	var statusPopup = new Popup('upload-status');
  statusPopup.hide();

	function showError(message) {
	    statusPopup.show({
	            title : 'Error',
	            message : message,
	            buttons : [ {
	                title : 'Dismiss',
	            action : function() {
                window.onbeforeunload = null;
                location.reload();
	                        }
	                    } ]
	                });
	}

	function checkFileTypes(fileinput, mask) {
		if (!fileinput || fileinput.blank()) {
 			return false;
 		}
 		var ext = fileinput.substring(fileinput.lastIndexOf('.'));
 		ext = ext.toLowerCase();
 		mask = mask.toLowerCase();
 		if (ext != mask) {
			return false;
		}
 		return true;
	}

	var UploadAll = function(event) {
		event.stop();
		return SendConfigurationFiles();
	}

	var UploadNew = function(event) {
		event.stop();
		return SendConfigurationFiles();
	}

	var UploadAllFiles = function (file_list, list_index) {
	    //file_list.forEach(function(file_name) {
	      try {
          var file_name = file_list[list_index];
	        var stream = ss.createStream();
	        var UploadFile = document.getElementById(file_name).files[0];
	        ss(socket).emit('file-upload', stream, {name: UploadFile.name , size: UploadFile.size});
	        var blobStream = ss.createBlobReadStream(UploadFile);
	        var size = 0;
          showUploadProgressPopUp(UploadFile.name, stream, file_list, list_index);
          IsUploadRunning = true;
	        blobStream.on('data', function(chunk) {
	        size += chunk.length;
    		  var progress = Math.floor(size / UploadFile.size * 100);
          updateProgress(progress);
        	 });

        	blobStream.pipe(stream);

        	blobStream.on('end', function() {
            IsUploadRunning = false;
            list_index++;
            if(list_index < file_list.length){
              UploadAllFiles(file_list, list_index);
            }
            else {
    	        statusPopup.show({
                    title   : 'Uploaded Success',
                    message : ('<p style="text-align:center">'
                              + 'Files Uploaded successfully. Board will Reboot Now.</p>'),
                    buttons : [ {
                                    title : 'Continue',
                                    action : function() {
                                      alert("Board will Reboot..Add Reboot functionality");
                                    }
                                },
                                {
                                  title : 'Discard',
                                  action : function() {
                                    window.onbeforeunload = null;
                                    location.reload();
                                  }
                                }
                              ]
              });
            }
          });

        	blobStream.on('error', function() {
          IsUploadRunning = false;
          blobStream.end();
        	statusPopup.show({
                    title : 'Board Generated Errors Encountered',
                    message : '<p style="text-align:center">' + progressMessage + '</p>',
                    buttons : [ {
                        title : 'Dismiss',
                        action : function() {
                        location.reload();
                        window.onbeforeunload = null;
                    }
                  } ]
              });
          });
      }
  catch(err) {
    window.onbeforeunload = null;
    showError('File uploading error. Please retry.');
  }
//});
}

socket.on('disconnect', function(){
  if(IsUploadRunning){
  statusPopup.hide();
  statusPopup.show({
            title : 'Network Error',
            width : '370px',
            message : '<p style="text-align:center">The server is taking too long to respond or something is wrong with your internet connection. Please try again later.</p>',
            buttons : [ {
                title : 'OK',
                action : function() {
                statusPopup.hide();
                location.reload();
            }
          } ]
      });
    }
  });

	var SendConfigurationFiles = function() {
	    var ExecFile = $F('exec-filename');
	    var AppFile = $F('app-filename');
	    var ConfFile = $F('conf-filename');
	    var file_list = new Array();
    if ( !ExecFile && !AppFile && !ConfFile )
		{
			showError('No files selected. Please select at least one file.');
			return;
		}

    var uploadExec = checkFileTypes(ExecFile, '.tar');
		if ( ExecFile )
		{
      file_list.push('exec-filename');
      if(!uploadExec){
			     showError('Executive File Type is not correct. Please choose .tar extension');
			        return;
            }
		}
    var uploadApp = checkFileTypes(AppFile, '.mlp');

    if (AppFile)
		{
      file_list.push('app-filename');
      if(!uploadApp){
		showError('Application File Type is not correct. Please choose .mlp extension');
	        return;
	    }
    }

    var uploadConf = checkFileTypes(ConfFile, '.json');
		if ( ConfFile )
		{
	      file_list.push('conf-filename');
      		if(!uploadConf){
		     showError('Configuration File Type is not correct. Please choose .json extension');
		     return;
          	}
	}
    UploadAllFiles(file_list, 0);
  }

  function showUploadProgressPopUp(fileName, stream, file_list, list_index){
    statusPopup.show({
          title : 'Please Wait .. Uploading ' + fileName,
          message : ('<p style="text-align:center">'
              + '<div id="myProgress"><div id="myBar"></div></div></p>'
              + '<p style="margin-top:5px" id="demo">0%</p>'),
             buttons : [{
                     title : 'Cancel',
                     action : function() {
                       stream.destroy();
                       IsUploadRunning = false;
                       socket.emit('delete_cancelled_file', { name : fileName});
                       list_index++;
                       if(list_index < file_list.length){
                         UploadAllFiles(file_list, list_index);
                       }
                       else{
                         statusPopup.hide();
                      }
                   }
             }]
           });
  }

  function updateProgress(progress){
    document.getElementById('myBar').style.width = progress + "%";
    document.getElementById("demo").innerHTML = progress  + '%';
  }

document.observe("dom:loaded", function() {
  ClearExecForm();
  ClearAppForm();
  ClearConfigForm();
  var edit_mode = location.search.split('edit_mode=')[1];
  if (!$('edit_mode')){
    statusPopup.show({
          title : 'Authenticated User Error',
          message : '<p style="text-align:center">' + 'This feature is only available to Authenticated User. Please login to Upload Software.' + '</p>',
          buttons : [ {
            title : 'Dismiss',
            action : function() {
              window.location.href = '/config';
              window.onbeforeunload = null;
            }
          } ]
        });
  return;
  }

  function ClearExecForm() {
		$('params-exec').innerHTML = '<input type="file" id="exec-filename" name="datafile" accept = ".tar" class="fchooser-file">';
		stylizeUpload($('exec-filename'));
	}

  function ClearAppForm() {
		$('params-app').innerHTML = '<input type="file" id="app-filename" name="datafile" accept = ".mlp" class="fchooser-file">';
		stylizeUpload($('app-filename'));
	}
  function ClearConfigForm() {
		$('params-conf').innerHTML = '<input type="file" id="conf-filename" name="datafile" accept = ".json" class="fchooser-file">';
		stylizeUpload($('conf-filename'));
	}


	if ($('upload_new_btn')) {
		$('upload_new_btn').observe('click', UploadNew);
	}

  	if ($('upload_all_btn')) {
		$('upload_all_btn').observe('click', UploadAll);
	}

	if ($('clear-exec-filename')) {
		$('clear-exec-filename').observe('click', ClearExecForm);
	}

	if ($('clear-app-filename')) {
		$('clear-app-filename').observe('click', ClearAppForm);
	}

	if ($('clear-conf-filename')) {
		$('clear-conf-filename').observe('click', ClearConfigForm);
	}
  });
})();
