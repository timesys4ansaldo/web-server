/* Setup text/graph mode changing */
(function() {
	var socket = io();
	save_page = "Free Run Logic File\r\n";
	save_filename = "Logic";
	var sort = 0;
	var stream;

	function getSessionVariables(data) {
		bitlist = data.Bit.bits;
		grouplist = data.Group.groups;
		updateVarList();
	}

	var stat_graph_template = "";
	var stat_timeline_template = "";
	function getGraphTemplate() {
		if (stat_graph_template.length == 0) {
			for (var iter = 0; iter < 60; iter++) {
				stat_graph_template += '<div class="timestate ts-no"></div>';
			}
		}
		return stat_graph_template;
	}
	function getTimelineTemplate() {
		if (stat_timeline_template.length == 0) {
			for (var iter = 0; iter < 60; iter++) {
				stat_timeline_template += '<div class="timestate ts-no">:'+(iter<10?"0":"")+iter+'</div>';;
			}
		}
		return stat_timeline_template;
	}
// updateVarList --Start--

function updateVarList() {
		var bitandvartable = $('bitandvars-table-tbody-1');
			for(var i = bitandvartable.rows.length - 1; i >= 0; i--){
				if (bitandvartable.rows[i].id != "delimiter") {
					bitandvartable.deleteRow(i);
				}
			}
		var iter = 0;
		$('graph_template').innerHTML = getGraphTemplate();
		$('horscroller').innerHTML = getTimelineTemplate();
		$('horscroller1').innerHTML = $('horscroller').innerHTML;

		if (activeIndex == -1){
			activeIndex = 0;
			activeGroup = grouplist[activeIndex].name;
		}
		// TODO: sort group bits if required - don't required
		// add active group bits before delimiter
		//grouplist[activeIndex].bitarray = grouplist[activeIndex].bitarray.sort(function(a, b){return a - b});
		for(var groupBitindex=0; groupBitindex < grouplist[activeIndex].bitarray.length; groupBitindex++) {
			var groupBitId = grouplist[activeIndex].bitarray[groupBitindex] -1 ;
			var newrow = bitandvartable.insertRow(iter);
			newrow.className = "intable" ;//+ (iter%2?" alt":"");
			newrow.id = "row_" + grouplist[activeIndex].bitarray[groupBitindex];
			var cell0 = newrow.insertCell(0);
			var cell1 = newrow.insertCell(1);
			var cell2 = newrow.insertCell(2);
			var cell3 = newrow.insertCell(3);
			var cell4 = newrow.insertCell(4);
			cell0.innerHTML = grouplist[activeIndex].bitarray[groupBitindex];
			cell0.className = "bitandvars-table-id";
			cell1.innerHTML = '<span id="span_'+grouplist[activeIndex].bitarray[groupBitindex]+'">'+bitlist[groupBitId]+'</span>';
			cell1.title = bitlist[groupBitId];
			cell1.id = "cell_" + grouplist[activeIndex].bitarray[groupBitindex];
			cell1.className = "bitandvars-table-symbol";
			cell2.innerHTML += '<a id="on_'+grouplist[activeIndex].bitarray[groupBitindex]+'" class="little-command-button" href="#" onclick="return false;">off</a>';
			cell2.innerHTML += '<a id="up_'+grouplist[activeIndex].bitarray[groupBitindex]+'" class="little-command-button" href="#" onclick="return false;">up</a>';
			cell2.innerHTML += '<a id="dn_'+grouplist[activeIndex].bitarray[groupBitindex]+'" class="little-command-button" href="#" onclick="return false;">dn</a>';
			cell2.className = "bitandvars-table-commands";
			cell2.id = "commands_"+grouplist[activeIndex].bitarray[groupBitindex];
			cell3.innerHTML = "";
			cell3.className = "bitandvars-table-value";
			cell3.id = "value_"+grouplist[activeIndex].bitarray[groupBitindex];
			cell4.innerHTML = '<div style="z-index: -1;overflow: hidden;"><div id="toscroll_'+grouplist[activeIndex].bitarray[groupBitindex]+'" class="toscroll">'+getGraphTemplate()+'</div></div>';
			cell4.className = "bitandvars-table-graph";

			var nodes = $("toscroll_"+grouplist[activeIndex].bitarray[groupBitindex]).childNodes;
			for(var i=0; i<nodes.length; i++) {
				if (nodes[i].nodeName.toLowerCase() == 'div') {
					 nodes[i].className = "timestate ts-off";
				 }
			}
			$("on_"+grouplist[activeIndex].bitarray[groupBitindex]).observe("click",
																	function() {
																		var elem = this;
																		addBitToGroup(elem);
																	});
			$("up_"+grouplist[activeIndex].bitarray[groupBitindex]).observe("click", function(){ moveAndSave(this); });
			$("dn_"+grouplist[activeIndex].bitarray[groupBitindex]).observe("click", function(){ moveAndSave(this); });
			iter++;
		}
		// TODO: sort inactive bits if required
		// add inactive bits after delimiter
		var delimiterrow = bitandvartable.rows[iter];
		var rowspan = 1;
		//for(var strName in bitlist) {
		storeBitArr = grouplist[activeIndex].inactive_bits;
		for (var arriter = 0; arriter < storeBitArr.length; arriter++) {
			var strName = storeBitArr[arriter];
				iter++;
				rowspan++;
				var newrow = bitandvartable.insertRow(iter);
				newrow.className = "intable";// + (iter%2?" alt":"");
				newrow.id = "row_" + storeBitArr[arriter];
				var cell0 = newrow.insertCell(0);
				var cell1 = newrow.insertCell(1);
				var cell2 = newrow.insertCell(2);
				var cell3 = newrow.insertCell(3);
				//var cell4 = newrow.insertCell(4);
				cell0.innerHTML = storeBitArr[arriter];
				cell0.className = "bitandvars-table-id";
				cell1.innerHTML = '<span id="span_'+storeBitArr[arriter]+'">'+bitlist[strName -1]+'</span>';
				//if ($("span_"+bitlist[strName].id).getWidth() > 190) {
					cell1.title = bitlist[strName -1];
				//}
				cell1.id = "cell_" + storeBitArr[arriter];
				cell1.className = "bitandvars-table-symbol";
				cell2.innerHTML += '<a id="on_'+storeBitArr[arriter]+'" class="little-command-button" href="#" onclick="return false;">on</a>';
				cell2.innerHTML += '<a id="up_'+storeBitArr[arriter]+'" class="little-command-button" href="#" onclick="return false;">up</a>';
				cell2.innerHTML += '<a id="dn_'+storeBitArr[arriter]+'" class="little-command-button" href="#" onclick="return false;">dn</a>';
				cell2.className = "bitandvars-table-commands";
				cell2.id = "commands_"+storeBitArr[arriter];
				//cell3.innerHTML = bitlist[strName -1];
				cell3.className = "bitandvars-table-value";
				cell3.id = "value_"+storeBitArr[arriter];
				//cell4.innerHTML = '<div style="z-index: -1;overflow: hidden;"><div id="toscroll_'+bitlist[strName].id+'" class="toscroll">'+getGraphTemplate()+'</div></div>';
				//cell4.className = "bitandvars-table-graph";
				$("on_"+storeBitArr[arriter]).observe("click",
																	function() {
																		var elem = this;
																		pause_while_edit_request = true;
																		addBitToGroup(elem);
																	});
				$("up_"+storeBitArr[arriter]).observe("click", function(){ moveAndSave(this); });
				$("dn_"+storeBitArr[arriter]).observe("click", function(){ moveAndSave(this); });
		}
		grouplist[activeIndex].inactive_bits = storeBitArr;
		filter1000();
		delimiterrow.cells[4].rowspan = rowspan;
		// update graph visualization
		ChangeMode(0);
		// update group list
		var bitandvarrow = $('group-row-container');
		for(var i = bitandvarrow.cells.length - 1; i >= 0; i--){
			bitandvarrow.deleteCell(i);
		}
		var celliter = 0;
		for(var iter = 0; iter < grouplist.length; iter++) {
			var grpName = grouplist[iter].name;
			var newcell = bitandvarrow.insertCell(celliter);
			if (grouplist[iter].visible == "0") {
				newcell.setStyle({
				  display: 'none'
				});
			}
			newcell.id = "td_" + grpName;
			//tmpGroup = activeGroup;
			newcell.innerHTML = '<a id="group_'+grpName+'" class="group-button'+(grpName==activeGroup?"-active":"")+'" href="#" onclick="return false;">'+grpName+'</a>';
			$('group_'+grpName).observe("click", function(){ activateGroup(this); });
			celliter++;
		}
	}

//Add Remove all Bits & to Active Group
	function updateAllBitsOfGroup(elem){
		if(!activeGroup){
			ShowAlert("No active Group..!!");
			return;
		}
		if(elem.textContent == 'ALL'){
			grouplist[activeIndex].bitarray = grouplist[activeIndex].bitarray.concat(grouplist[activeIndex].inactive_bits);
			grouplist[activeIndex].inactive_bits.splice(0,grouplist[activeIndex].inactive_bits.length);
		}
		else {
			grouplist[activeIndex].inactive_bits = grouplist[activeIndex].bitarray.concat(grouplist[activeIndex].inactive_bits);
			grouplist[activeIndex].bitarray.splice(0,grouplist[activeIndex].bitarray.length);
			grouplist[activeIndex].msg.splice(0,grouplist[activeIndex].msg.length);
		}
		updateVarList();
		socket.emit('all_bit_status',{"grp_id" : activeIndex, 'oper' : elem.textContent});
	}

	socket.on('all_received_bit_status' , function (bit_info){
		grouplist[activeIndex].msg = bit_info.msg;
	});

	socket.on('received_bit_status' , function (bit_info){
		grouplist[activeIndex].msg.push(bit_info.msg);
		grouplist[activeIndex].txt += bit_info.txt;
		$var_mon_data_field = grouplist[activeIndex].txt;
		socket.emit("edit-bitgroup", {"group_index" : activeIndex , 'active_bits' : grouplist[activeIndex].bitarray,
		'inactive_bits' : grouplist[activeIndex].inactive_bits, 'msg' : grouplist[activeIndex].msg});
	});

	function addBitToGroup(elem) {
		if(!activeGroup){
			ShowAlert("No active Group..!!");
			return;
		}
		var tmpId = elem.id.substring(3);
		var action = (elem.innerHTML=="on") ? "on" : "off";
		// add selected bit to Active Group
			if(action == "on"){
				socket.emit('bit_status',{'bit_id' : tmpId});
				var inactive_bit_index = grouplist[activeIndex].inactive_bits.indexOf(parseInt(tmpId));
				grouplist[activeIndex].bitarray.push(parseInt(tmpId));
				grouplist[activeIndex].inactive_bits.splice(inactive_bit_index, 1);
				//MAITS
				var target = document.getElementById('delimiter');
				var to_add = document.getElementById("row_" + tmpId);
				target.parentNode.insertBefore(to_add, target);
				var thisrow = $('row_' + tmpId);
				var cell4 = thisrow.insertCell(4);
				cell4.innerHTML = '<div style="z-index: -1;overflow: hidden;"><div id="toscroll_'+tmpId+'" class="toscroll">'+getGraphTemplate()+'</div></div>';
				cell4.className = "bitandvars-table-graph";
				var nodes = $("toscroll_"+tmpId).childNodes;
				for(var i=0; i<nodes.length; i++) {
					if (nodes[i].nodeName.toLowerCase() == 'div') {
						 nodes[i].className = "timestate ts-off";
					 }
				}
				$('delimiter_td').rowspan -= 1;
				$("on_" + tmpId).innerHTML = "off";
				// update graph visualization
				ChangeMode(0);
				$("horscroller").onscroll();
				//MAITS
			}
			// remove selected bit from Active Group
			else {
				var active_bit_index = grouplist[activeIndex].bitarray.indexOf(parseInt(tmpId));
				grouplist[activeIndex].bitarray.splice(active_bit_index, 1);
				grouplist[activeIndex].msg.splice(active_bit_index, 1);
				grouplist[activeIndex].inactive_bits.push(parseInt(tmpId));
				grouplist[activeIndex].inactive_bits = grouplist[activeIndex].inactive_bits.sort(function (a, b) {  return a - b;  });
				var target = document.getElementById('delimiter');
				var to_add = document.getElementById("row_" + tmpId);
					target.parentNode.insertBefore(to_add,target.nextSibling);
					$("on_" + tmpId).innerHTML = "on";
					var thisrow = $('row_' + tmpId);
					thisrow.deleteCell(4);
					$("value_"+tmpId).innerHTML="";
					$('delimiter_td').rowspan += 1;
			}
			socket.emit("edit-bitgroup", {"group_index" : activeIndex , 'active_bits' : grouplist[activeIndex].bitarray, 'inactive_bits' : grouplist[activeIndex].inactive_bits, 'msg' : grouplist[activeIndex].msg});
	}


	function ChangeMode(mode) {
		/* mode = 0 for update current mode
			 mode = 1 for text
			 mode = 2 for graph */
		if (mode == 1) {
			$('list-table-cell').colSpan = "4";
			var graph_cells = $$('.bitandvars-table-graph');
			graph_cells.each(function(el) {
				el.setStyle({
					display : 'none'
				});
			});
			var text_cells = $$('.bitandvars-table-textdata');
			text_cells.each(function(el) {
				el.setStyle({
					display : 'table-cell'
				});
			});
		} else {
			if (mode == 2) {
				$('list-table-cell').colSpan = "5";
				var graph_cells = $$('.bitandvars-table-graph');
				graph_cells.each(function(el) {
					el.setStyle({
						display : 'table-cell'
					});
				});
				var text_cells = $$('.bitandvars-table-textdata');
				text_cells.each(function(el) {
					el.setStyle({
						display : 'none'
					});
				});
				$("horscroller").scrollLeft = $("horscroller").scrollWidth;
				$("horscroller").onscroll();
			} else {
				if ($('info_mode_btn').innerHTML == "Text") {
					ChangeMode(2);
				} else {
					ChangeMode(1);
				}
			}
		}
	}

	function onChangeMode() {
		var ModeButton = $('info_mode_btn');
		if (ModeButton.innerHTML == "Text")
		{
			ModeButton.innerHTML = "Graphical";
			ChangeMode(1);
		} else {
			ModeButton.innerHTML = "Text";
			ChangeMode(2);
		}
	}
	$('info_mode_btn').observe('click', onChangeMode);

	$('horscroller').onscroll = function () {
		var toscrollDivs = $$('.toscroll');
		toscrollDivs.each(function(el) {
			el.style.margin = "0 0 0 " + $("horscroller").scrollLeft*-1 + "px";
		});
		$('horscroller1').style.margin = "0 0 0 " + $("horscroller").scrollLeft*-1 + "px";
	}

	// setup monitoring functionality
	var pause_while_edit_request = false;
	var data_request_id = 0;
	var activeGroup = null;
	var activeIndex = -1;
	var firstStartTime = 0;
	var lastUpdateTime = 0;
	var firstTimeLineVal = "";
	var bitlist = [];
	var grouplist = [];
	var save_text = "";
	var pause_bool = false;
	var pause_time;
	var start_time;

	function addStopColumnState(new_time) {
		// TODO remove old val-colums if required: node.parentNode.removeChild(node)
		var timeparam = new_time.toString();
		if (timeparam.length < 2) {
			timeparam = '0' + timeparam;
		}

		for(var groupBitindex=0; groupBitindex < grouplist[activeIndex].bitarray.length; groupBitindex++) {
			var groupBitId = grouplist[activeIndex].bitarray[groupBitindex];
			// remove first childN
			var parentNode = $('toscroll_'+grouplist[activeIndex].bitarray[groupBitindex]);
			var nodes = parentNode.childNodes;
			for(var i=0; i<nodes.length-1; i--) {
				if (nodes[i].nodeName.toLowerCase() == 'div') {
					parentNode.removeChild(nodes[i]);
					break;
				}
			}
			// duplicate last state
			for(var i=nodes.length-1; i>=0; i--) {
				if (nodes[i].nodeName.toLowerCase() == 'div') {
					var cln = nodes[i].cloneNode(true);
					if ( (cln.className == "timestate ts-hi-on") || (cln.className == "timestate ts-hi-up") || (cln.className == "timestate ts-hi-go")) {
						cln.className = "timestate ts-hi-of";
					} else {
						if ( (cln.className == "timestate ts-lo-on") || (cln.className == "timestate ts-lo-dn") || (cln.className == "timestate ts-lo-go") ) {
							cln.className = "timestate ts-lo-of";
						} else {
							if ( (cln.className == "timestate ts-md-on") || (cln.className == "timestate ts-md-go") ) {
								cln.className = "timestate ts-md-of";
							}
						}
					}
					parentNode.appendChild(cln);
					break;
				}
			}

		}
		$('horscroller').innerHTML += '<div class="timestate ts-no">:'+timeparam+'</div>';
		var updNode = $('horscroller');
		var updNodes = updNode.childNodes;
		for(var i=0; i<updNodes.length-1; i--) {
			if (updNodes[i].nodeName.toLowerCase() == 'div') {
				updNode.removeChild(updNodes[i]);
				break;
			}
		}
		$('horscroller1').innerHTML = $('horscroller').innerHTML;
	}

	function addColumnStateToAllRows(new_time) {
		// TODO remove old val-colums if required: node.parentNode.removeChild(node)
		var timeparam = new_time.toString();
		if (timeparam.length < 2) {
			timeparam = '0' + timeparam;
		}

		for(var groupBitindex=0; groupBitindex < grouplist[activeIndex].bitarray.length; groupBitindex++) {
			var groupBitId = grouplist[activeIndex].bitarray[groupBitindex];
			// remove first childN
			var parentNode = $('toscroll_'+grouplist[activeIndex].bitarray[groupBitindex]);
			var nodes = parentNode.childNodes;
			for(var i=0; i<nodes.length-1; i--) {
				if (nodes[i].nodeName.toLowerCase() == 'div') {
					parentNode.removeChild(nodes[i]);
					break;
				}
			}
			// duplicate last state
			for(var i=nodes.length-1; i>=0; i--) {
				if (nodes[i].nodeName.toLowerCase() == 'div') {
					var cln = nodes[i].cloneNode(true);
					if ( (cln.className == "timestate ts-hi-on") || (cln.className == "timestate ts-hi-up") ) {
						cln.className = "timestate ts-hi-go";
					} else {
						if ( (cln.className == "timestate ts-lo-on") || (cln.className == "timestate ts-lo-dn") ) {
							cln.className = "timestate ts-lo-go";
						} else {
							if (cln.className == "timestate ts-md-on") {
								cln.className = "timestate ts-md-go";
							} else {
								if ( (cln.className == "timestate ts-md-of") || (cln.className == "timestate ts-lo-of") || (cln.className == "timestate ts-hi-of")) {
									cln.className = "timestate ts-off";
								}
							}
						}
					}
					parentNode.appendChild(cln);
					break;
				}
			}

		}
		$('horscroller').innerHTML += '<div class="timestate ts-no">:'+timeparam+'</div>';
		var updNode = $('horscroller');
		var updNodes = updNode.childNodes;
		for(var i=0; i<updNodes.length-1; i--) {
			if (updNodes[i].nodeName.toLowerCase() == 'div') {
				updNode.removeChild(updNodes[i]);
				break;
			}
		}
		$('horscroller1').innerHTML = $('horscroller').innerHTML;

	}

	function visualizeResponseMessage(mon_data) {
    var curUpdateTime = parseInt(mon_data.timestamp);
		if (firstStartTime == 0) {
			firstStartTime = curUpdateTime;
			var boardtimeinput = $('board-time').value;
			// initialize prev time line
			firstTimeLineVal = parseInt(boardtimeinput.substring(boardtimeinput.length-2));
			for (var iter = 0; iter < firstTimeLineVal; iter++) {
				addColumnStateToAllRows(iter);
			}
			addColumnStateToAllRows(firstTimeLineVal);
		} else {
			if (pause_bool){
				pause_bool = false;
				var limit = 0;
					if(pause_time && start_time){
						limit = (start_time - pause_time)/1000;
					} else {
						limit = curUpdateTime - lastUpdateTime;
					}
			for (var iter = 0; iter < limit; iter++) {
				firstTimeLineVal++;
				if (firstTimeLineVal == 60) {
					firstTimeLineVal = 0;
				}
				addColumnStateToAllRows(firstTimeLineVal);
			}
		} else {
			firstTimeLineVal++;
			if (firstTimeLineVal == 60) {
				firstTimeLineVal = 0;
			}
			addColumnStateToAllRows(firstTimeLineVal);
		}
		}
		lastUpdateTime = curUpdateTime;
			for (var i=0;i< grouplist[activeIndex].bitarray.length;i++) { // update values
				val = (grouplist[activeIndex].msg[i] == 0 ? '0' : grouplist[activeIndex].msg[i] == 1 ? "SET":"CLEAR");
				$( 'value_' + grouplist[activeIndex].bitarray[i]).innerHTML = val;
				// update graph - change last added state to correct
				var parentNode = $('toscroll_'+grouplist[activeIndex].bitarray[i]);
				if (parentNode) {
					var nodes = parentNode.childNodes;
					var lastNode;
					for(var k=nodes.length-1; k>=0; k--) {
						if (nodes[k].nodeName.toLowerCase() == 'div') {
							lastNode = nodes[k];
							break;
						}
					}
					if ( val == '0' ) {
						// TODO for integer values check if last is gray and change it to blue middle
						if ( (lastNode.className == "timestate ts-off") || (lastNode.className == "timestate ts-md-of") ) {
							lastNode.className = "timestate ts-md-on";
						}
					} else {
						// TODO for boolean values check if last is gray or not equal to new and change
					if ( val != "CLEAR" ) {
						if ( (lastNode.className == "timestate ts-off") || (lastNode.className == "timestate ts-lo-of") || (lastNode.className == "timestate ts-hi-of") ) {
							lastNode.className = "timestate ts-hi-on";
							} else {
								if ( (lastNode.className == "timestate ts-lo-on") || (lastNode.className == "timestate ts-lo-dn") || (lastNode.className == "timestate ts-lo-go") ) {
									lastNode.className = "timestate ts-hi-up";
								}
							}
						} else {
							if ( (lastNode.className == "timestate ts-off") || (lastNode.className == "timestate ts-lo-of") || (lastNode.className == "timestate ts-hi-of") ) {
								lastNode.className = "timestate ts-lo-on";
							} else {
								if ( (lastNode.className == "timestate ts-hi-on") || (lastNode.className == "timestate ts-hi-up") || (lastNode.className == "timestate ts-hi-go") ) {
									lastNode.className = "timestate ts-lo-dn";
								}
							}
						}
				}
			}
		}

	$("horscroller").scrollLeft = $("horscroller").scrollWidth;
	}

	socket.on('received_mon_data',function(mon_data){
				visualizeResponseMessage(mon_data);
	});
	function monStartStop() {
		pause_while_edit_request = false;
		var button = $('info_pause_btn');
		if ( button.innerHTML == "START" ) {
					start_time = new Date().getTime();
					button.innerHTML = "PAUSE";
					//TODO: disable group buttons ???
					grouplist[activeIndex].txt = "";
					for(var groupBitindex=0; groupBitindex < grouplist[activeIndex].msg.length; groupBitindex++) {
						if(grouplist[activeIndex].msg[groupBitindex] == 0){
							grouplist[activeIndex].txt += bitlist[grouplist[activeIndex].bitarray[groupBitindex] - 1] + " is UNSET \n";
						}
						else if(grouplist[activeIndex].msg[groupBitindex] == 1)
						{
							grouplist[activeIndex].txt += bitlist[grouplist[activeIndex].bitarray[groupBitindex] - 1] + " is SET \n";
						}
						else {
							grouplist[activeIndex].txt += bitlist[grouplist[activeIndex].bitarray[groupBitindex] - 1] + " is CLEAR \n";
						}
					}
						$("var_mon_data_field").value += grouplist[activeIndex].txt;
						var date = new Date();
						$("var_mon_data_field").value += "\r\n Current value(s) retrieved.";
						$("var_mon_data_field").value += "\n(PC: " +	date + ") \n Processing time: \r\n\n";
						$("var_mon_data_field").scrollTop = $("var_mon_data_field").scrollHeight;
						socket.emit('get-varmonitor');
		} else {
			pause_bool = true;
			pause_time = new Date().getTime();
			button.innerHTML = "START";
			socket.emit('stop-varmonitor');
			firstTimeLineVal++;
			addStopColumnState(firstTimeLineVal);
			$("horscroller").scrollLeft = $("horscroller").scrollWidth;
		}
	}

	function monClean() {
		// TODO: if monitoring started - no bits while status
		var button = $('info_pause_btn');
		if (button.innerHTML == "START") {
			$("var_mon_data_field").innerHTML = "";
			updateVarList();
			document.getElementById('var_mon_data_field').value = "";
			firstStartTime = 0;
			$("horscroller").onscroll();
			} else {
				ShowAlert("Please stop monitoring before clearing log.");
		}
	}

	function throttle(func, ms) {
		var isThrottled = false,
		savedArgs,
		needToFinish = false,
		savedThis;

		function wrapper() {
			if (isThrottled) { // (2)
				savedArgs = arguments;
				savedThis = this;
				needToFinish = true;
				return;
			}
			func.apply(this, arguments); // (1)
			isThrottled = true;
			setTimeout(function() {
				isThrottled = false; // (3)
				if (needToFinish) {
					wrapper.apply(savedThis, savedArgs);
					needToFinish = false;
					savedArgs = savedThis = null;
				}
			}, ms);
		}
		return wrapper;
	}

	function filter_vars() {
		// get filter
		var filter_msg = $('find_field').value.trim().toUpperCase();
		if (!filter_msg) {
			$$('.intable')
			var rows = $$('.intable');
			rows.each(function(el) {
				el.show();
			});
			return;
		}
		var iter = 1;
		var cur_row = $('row_'+iter);
		var cur_cell = $('cell_'+iter);
		while (cur_row != null) {
			if (cur_cell.innerHTML.toUpperCase().indexOf(filter_msg) > -1) {
				cur_row.show();
			} else {
				cur_row.hide();
			}
			//next row
			iter++;
			cur_row = $('row_'+iter);
			cur_cell = $('cell_'+iter);
		}
	}
	var filter1000 = throttle(filter_vars, 1000);

	var sortcolumn = -1;/* 0 - id, 1 - name, -1 - none */
	var sortdirect = 1;/* 1 - asc, -1 - desc */

	function updateArrow() {
		var uptext = (sortdirect > 0)?"":"up";
		var arrow = '<img src="public/img/arrow' + uptext + '.png">'
		for (var i = 0; i <=1; i++) {
			var elem = $('sortflag_palceholder_'+i);
			if (i == sortcolumn) {
				elem.innerHTML = arrow;
			} else {
				elem.innerHTML = "";
			}
		}
	}

	function setSort(colnum) {
		if (sortcolumn == colnum) {
			sortdirect *= -1;
		} else {
			sortcolumn = colnum;
			sortdirect = 1;
		}
		if (colnum >= 0) {
			var curGroup = grouplist[activeIndex].bitarray.slice();

			var inactive_bits = grouplist[activeIndex].inactive_bits.slice();
			//sort
			curGroup.sort(function(a,b) {
					var item1 = bitlist[a-1];
					var item2 = bitlist[b-1];
					var num1 = bitlist.indexOf(item1);
					var num2 = bitlist.indexOf(item2);
					if (sortcolumn == 0) {
						item1 = num1;
						item2 = num2;
					}
					if (item1 > item2) return sortdirect;
					if (item1 < item2) return -1*sortdirect;
					if (num1 > num2) return sortdirect;
					if (num1 < num2) return -1*sortdirect;
					return 0;
				});
				inactive_bits.sort(function(a,b) {
						var item1 = bitlist[a-1];
						var item2 = bitlist[b-1];
						var num1 = bitlist.indexOf(item1);
						var num2 = bitlist.indexOf(item2);
						if (sortcolumn == 0) {
							item1 = num1;
							item2 = num2;
						}
						if (item1 > item2) return sortdirect;
						if (item1 < item2) return -1*sortdirect;
						if (num1 > num2) return sortdirect;
						if (num1 < num2) return -1*sortdirect;
						return 0;
					});
				grouplist[activeIndex].bitarray = curGroup;
				grouplist[activeIndex].inactive_bits = inactive_bits;

				updateVarList();
				updateArrow();
				socket.emit('edit-bitgroup',{"group_index": activeIndex, "active_bits": curGroup, "inactive_bits" : inactive_bits });
		}else {
			updateArrow();
		}
	}

	document.observe("dom:loaded", function() {
		// init var monitor on board
		$('sort_id').observe('click',function() {
			setSort(0);
		});
		$('sort_name').observe('click',function() {
			setSort(1);
		});
		socket.emit('init-var-monitor');
		socket.on("init-var-monitor-response", function(data){
				if (data) {
					var status = data.status;
					if (status == "0") {
						getSessionVariables(data);
					} else {
						if (status == "-1"){
							message : html,
							ShowAlert(data.descr);
						} else {
							ShowAlert("Initialization error. Please refresh the page to try again.");
						}
					}
				} else {
					ShowAlert("Initialization error. Please refresh the page to try again.");
				}
			});

		$("info_pause_btn").observe("click", monStartStop );
		$("info_clear_btn").observe("click", monClean );
		$("info_none_btn").observe("click", function(){ updateAllBitsOfGroup(this); });
		$("info_all_btn").observe("click", function(){ updateAllBitsOfGroup(this); });
		$("info_groups_btn").observe("click", editGroupList );
		$("info_save_btn").observe("click", save_data);
		var find_field = $("find_field");
		find_field.value = "";
		find_field.onkeyup = find_field.oninput = filter1000;
		find_field.onpropertychange = function() {
			if (event.propertyName == "value") filter1000();
		}
		find_field.oncut = function() {
			setTimeout(filter1000, 0); // на момент oncut значение еще старое
		};
	});

	var createBtnInitialized = false;
	var groupCount = 0;

	function editGroupList() {
		var button = $('info_pause_btn');
		if (button.innerHTML == "START") {
			groupListPopup.show({
					title : 'Symbol Groups',
					message : html,
					buttons : [ {
					title : 'Done'
				} ]
			});
			var groupTable = $('group-list-tbody');
			for(var i = groupTable.rows.length - 2; i > 0; i--){
				groupTable.deleteRow(i);
			}
			groupCount = 0;
			for(var iter = 0; iter < grouplist.length; iter++) {
				var grpName = grouplist[iter].name;
				var grpVisible = "";
				if (grouplist[iter].visible == "1") {
					grpVisible = " checked";
				}
				var newrow = groupTable.insertRow(groupCount);
				newrow.id = "group_row_" + grpName;
				var cell0 = newrow.insertCell(0);
				var cell1 = newrow.insertCell(1);
				var cell2 = newrow.insertCell(2);
				cell0.innerHTML = '<input id="show_grp_' + grpName + '" name="show_grp_' + grpName + '" type="checkbox"' + grpVisible + ' style="float: left; margin: 2px;" class="cfgproc fix-ibox-padding">';
				$('show_grp_'+grpName).onclick = changeGroupVisualisation;
				cell1.innerHTML = grpName;
				cell2.setStyle({
						padding : '4px'
					});
				cell2.innerHTML = '<a id="group_delete_btn_'+grpName+'" class="button" href="#" onclick="return false;">Delete</a>';
				$("group_delete_btn_"+grpName).observe("click", function(){ deleteGroup(this); });
				groupCount++;
			}
			if (!createBtnInitialized) {
				$('group_create_btn').observe("click", createGroup );
			}
		} else {
			ShowAlert("Please stop monitoring before editing group list.");
		}
	}

	function deleteGroup(elem) {
		groupCount = grouplist.length;
		if (groupCount < 2) {
			ShowAlert("You must have at least one group. Create a new one before deleting this group.");
		} else {
			var grpname = elem.id.substring(17);
			ShowAlert("Going to Delete " + grpname + " Active group is " + activeGroup);
			if (grpname == grouplist[activeIndex].name) {
				ShowAlert("You cannot delete active group.");
				return;
			}
			var grpIndex = -1;
			for (var i=0;i<grouplist.length;i++){
				if(grpname == grouplist[i].name){
					grpIndex = i;
				}
			}
			grouplist.splice(grpIndex,1);
			// Update Active index
			activeIndex = 0;
			while (activeIndex < grouplist.length && grouplist[activeIndex].name != activeGroup ) {
				activeIndex++;
			}

			socket.emit("remove-bitgroup", {"group_index" : grpIndex});
			// remove group from group panel
			var bitandvarrow = $('group-row-container');
			var cell = $("td_"+grpname);
			if (cell) {
				bitandvarrow.deleteCell(cell.cellIndex);
			}
			var grouprowpopup = $('group-list-table');
			var row = $("group_row_"+grpname);
			if (row) {
				grouprowpopup.deleteRow(row.rowIndex);
			}
			updateVarList();
		}
  }

	function moveAndSave(elem , active) {
		var tmpId = elem.id.substring(3);
		var check = document.getElementById("on_" + tmpId );
		var array = [];
		if (check.textContent == 'off'){
			array =  	grouplist[activeIndex].bitarray;
		}
		else {
			array = grouplist[activeIndex].inactive_bits;
		}
		var index_to_move = array.indexOf(parseInt(tmpId));
		var bitarray_last_index = array.length - 1;

		if(index_to_move == 0 && elem.textContent == "up"){
			ShowAlert("cannot move first element any further");
			return;
		}
		else if (index_to_move == bitarray_last_index && elem.textContent == "dn"){
			ShowAlert("cannot move last element further down");
			return;
		} else {
			var arr = arrayElemMove(array,index_to_move,elem,check.textContent);
			if(check.textContent == 'off'){
				grouplist[activeIndex].bitarray = arr;

			} else {
				grouplist[activeIndex].inactive_bits = arr;
			}
		}
		socket.emit("edit-bitgroup", {"group_index" : activeIndex , 'active_bits' : grouplist[activeIndex].bitarray, 'inactive_bits' : grouplist[activeIndex].inactive_bits, 'msg' : grouplist[activeIndex].msg});
	}

	function arrayElemMove(array, elem_index, elem, inactive){
	    var element = array[elem_index];
	    array.splice(elem_index, 1);
			var row = elem.parentNode.parentNode,
			sibling = row.previousElementSibling,
			anchor = row.nextElementSibling,
			parent = row.parentNode;
	    if ( elem.textContent == 'up'){
				if (inactive == 'off'){
					var el = grouplist[activeIndex].msg[elem_index];
					grouplist[activeIndex].msg[elem_index] = grouplist[activeIndex].msg[elem_index - 1];
					grouplist[activeIndex].msg[elem_index - 1] = el;
				}
	        array.splice(elem_index - 1, 0, element);
    			parent.insertBefore(row, sibling);
	    }
	    else {
				if (inactive == 'off'){
					var el = grouplist[activeIndex].msg[elem_index];
					grouplist[activeIndex].msg[elem_index] = grouplist[activeIndex].msg[elem_index + 1];
					grouplist[activeIndex].msg[elem_index + 1] = el;
				}
	      array.splice(elem_index + 1, 0, element);
				parent.insertBefore( anchor, row);
	    }
			return array;
	}

	function createGroup() {
		groupCount = grouplist.length;
		if (groupCount >= 10) {
			ShowAlert("Maximum of 10 groups allowed. Please remove a group before adding a new one.");
		} else {
			var newgrp = $('newgrp').value;
			if (newgrp == '') {
				ShowAlert("Please enter group name.");
				return;
			}

			var index = 0;
			while(index < groupCount){
				if (grouplist[index].name == newgrp){
					ShowAlert("Group name already exists. Use another group name.");
					return;
				}
				index++;
			}
			checkGroupExist(newgrp, function(isValid){
				if(isValid){
					//var inactiveBit = inactiveBitRange(bitlist.length);
					var new_grp = {'name' : newgrp,'visible' : '1','bitarray' : [] , 'inactive_bits' : [], 'msg' : [], 'txt' : ''};
					grouplist.push(new_grp);
					grouplist[index].bitarray = grouplist[activeIndex].bitarray.slice();
					grouplist[index].inactive_bits = grouplist[activeIndex].inactive_bits.slice();
					grouplist[index].msg = grouplist[activeIndex].msg.slice();
					insertPopupRow(newgrp);
					updateVarList();
					socket.emit("add-bitgroup", {"new_grp" : grouplist[index]});
				}
				else {
					return;
				}
			});
	}
}

  function checkGroupExist(newgrp, callback){
		socket.emit('validate_symbol_group_exist', newgrp, function(result) {
			if(result){
				ShowAlert("oops!!Either group already exist or it exceeds max limit. Please refresh page to get latest groups");
				callback(false);
			}
			else
			  callback(true);
		});
		// socket.emit('validate_symbol_group_count', function(result){
		// 	if(result){
		// 		ShowAlert("Group count exceeds 10. Refresh page to get all new groups")
		// 		callback(false);
		// 	}
		// 	else {
		// 		callback(true);
		// 	}
		// });
	}


	function inactiveBitRange(end){
		return Array(end).join(0).split(0).map(function(val, id) {return id+1});
	}

	function insertPopupRow(newgrp) {
		$('newgrp').value = "";
		var groupTable = $('group-list-table');
		var newrow = groupTable.insertRow(groupCount);
		var grpVisible = " checked";
		newrow.id = "group_row_" + newgrp;
		var cell0 = newrow.insertCell(0);
		var cell1 = newrow.insertCell(1);
		var cell2 = newrow.insertCell(2);
		cell0.innerHTML = '<input id="show_grp_' + newgrp + '" name="show_grp_' + newgrp + '" type="checkbox"' + grpVisible + ' style="float: left; margin: 2px;" class="cfgproc fix-ibox-padding">';
		$('show_grp_'+newgrp).onclick = changeGroupVisualisation;
		cell1.innerHTML = newgrp;
		cell2.setStyle({
				padding : '4px'
			});
		cell2.innerHTML = '<a id="group_delete_btn_'+newgrp+'" class="button" href="#" onclick="return false;">Delete</a>';
		$("group_delete_btn_"+newgrp).observe("click", function(){ deleteGroup(this); });
	}

	function activateGroup(elem) {
		var button = $('info_pause_btn');
		if (button.innerHTML == "START") {
			var tmpId = elem.id.substring(6);
			if (activeGroup == tmpId) {
				return;
			} else {
				activeGroup = tmpId;
				activeIndex = 0;
				while (activeIndex < grouplist.length && grouplist[activeIndex].name != tmpId ) {
					activeIndex++;
				}
				updateVarList();
				$('horscroller').onscroll();
			}
		} else {
			ShowAlert("Please stop monitoring before changing group.");
		}
	}

	function changeGroupVisualisation() {
		var elem = this;
		var namegrp = elem.id.substring(9);
		var group_visibility = elem.checked ? "1" : "0";
		if ( namegrp == grouplist[activeIndex].name ) {
			ShowAlert("You cannot hide active group. Please activate another group before hiding current.");
			elem.checked = !elem.checked;
			return;
		}
		var grpIndex = -1;
		for (var i=0;i<grouplist.length;i++){
			if(namegrp == grouplist[i].name){
				grpIndex = i;
			}
		}

		grouplist[grpIndex].visible = group_visibility;
		updateVarList();
		socket.emit('visible-bitgroup', {"group_index": grpIndex,"visible": group_visibility});
	}

function save_data(){
	if(document.getElementById('var_mon_data_field').value == ""){
		ShowAlert('No data monitored to save. Try again after monitoring data.');
	}
	else{
		DownloadData (document.getElementById('var_mon_data_field').value, 'Logic.txt', 'application/json');
	}
}

	var groupListPopup = new Popup('groupListPopup');
	var html = '';
	html += '<div class="scroll clearfix fullheight-inner" style="height: 250px;">';
	html += '<table id="group-list-table" class="data-table">';
	html += '<thead><tr><th>Show</th><th>Group name</th><th>Actions</a></th></tr></thead>';
	html += '<tbody id="group-list-tbody">';
	html += '</tbody>';
	html += '<tfoot style="background-color: #ced4d9;">';
	html += '<tr><td></td>';
	html += '<td><input value="" type="text" id="newgrp" name="newgrp" maxlength="20"></td>';
	html += '<td style="padding: 4px;"><a id="group_create_btn" class="button" href="#" onclick="return false;">Create</a></td>';
	html += '</tr>';
	html += '</tfoot>';
	html += '</table>';
	html += '</div>';

})();
