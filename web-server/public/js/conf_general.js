var dropdownIDList = [];
var last_transport;
var socket = io();
var changePasswordPopup = new Popup('changePasswordPopup');

(function() {
  // Wait until dom loded

  socket.on('time', function(timeString) {
      if(document.getElementById('PCVCFG').innerHTML == "" && document.getElementById('PCNVCFG').innerHTML == ""){
  			var board_time = new Date(timeString);
        var time =  (board_time.getMonth()+1) + "/"
                  + board_time.getDate() + "/"
                  + board_time.getFullYear() + " "
                  + (board_time.getHours()<10?"0": (board_time.getHours() >= 12 ? (board_time.getHours() - 12 < 10 ? "0" + board_time.getHours() - 12 : board_time.getHours() - 12) : board_time.getHours())) + ":"
                  + (board_time.getMinutes()<10?"0":"") + board_time.getMinutes() + ":"
                  + (board_time.getSeconds()<10?"0":"") + board_time.getSeconds() + " "
                  + (board_time.getHours() >= 12 ? "PM" : "AM");
        document.getElementById('PCVCFG').innerHTML = time;
        document.getElementById('PCNVCFG').innerHTML = time;
      }
	});


  function BindData(){
    document.getElementById('lbl_current_logic_timeout').style.display = 'none';
    document.getElementById('lbl_current_synchronization_timeout').style.display = 'none';
    socket.emit('get_general_config_list', function(error, data) {
      if(error){
        ShowAlert("No data found for General Configuration.");
      }
      else{
        last_transport = data.ConfigList;
        var objGeneralConfig = GetFromLocalStorage('objGeneralConfig') ? GetFromLocalStorage('objGeneralConfig') : (data.configSettings != "" ? data.configSettings : false);
        //document.getElementById('PCVCFG').innerHTML = data.last_vital_configuration;
        //document.getElementById('PCNVCFG').innerHTML = data.last_non_vital_configuration;
        document.getElementById('txt_logic_timeout').disabled = document.getElementById('conf_save_changes_btn') ? false : true;
        if(document.getElementById('txt_logic_timeout').disabled)
          document.getElementById('div_LogicTimeout').classList.add('is-disabled');
        document.getElementById('txt_logic_timeout').value = objGeneralConfig ? objGeneralConfig.logicTimeout : data.configData.logicTimeout.dv;
        document.getElementById('lbl_current_logic_timeout').innerHTML = '[current: '+ (data.configSettings != "" ? data.configSettings.logicTimeout : data.configData.logicTimeout.dv) +']';
        document.getElementById('lbl_default_logic_timeout').innerHTML = "[default: "+ data.configData.logicTimeout.dv +"]";
        document.getElementById('txt_delay_reset').value = objGeneralConfig ? objGeneralConfig.delayReset : data.configData.delayReset.dv;
        document.getElementById('lbl_default_delay_reset').innerHTML = "[default: "+ data.configData.delayReset.dv +"]";
        document.getElementById('txt_synchronization_timeout').disabled = document.getElementById('conf_save_changes_btn') ? false : true;
        if(document.getElementById('txt_synchronization_timeout').disabled)
          document.getElementById('div_SynchronizationTimeout').classList.add('is-disabled');
        document.getElementById('txt_synchronization_timeout').value = objGeneralConfig ? objGeneralConfig.synchronizationTimeout : data.configData.synchronizationTimeout.dv;
        document.getElementById('lbl_current_synchronization_timeout').innerHTML = '[current: '+ (data.configSettings != "" ? data.configSettings.synchronizationTimeout : data.configData.synchronizationTimeout.dv) +']';
        document.getElementById('lbl_default_synchronization_timeout').innerHTML = "[default: "+ data.configData.synchronizationTimeout.dv +"]";
        data.configData.configList.forEach(function(item){
          dropdownIDList.push('select'+ item.id);
          var container = document.getElementById('options_list');
          var node = document.createElement('div');
          node.id = "div" + item.id;
          node.classList.add("general-configuration-dynamic-list-wrapper");
          node.innerHTML = '<label style="padding-top:5px;width:60px;vertical-align:middle;display:inline-block">Event '+ item.id +' : </label>'+
                           '<img style="padding: 5px 5px 0px 5px; margin-bottom: -2px; vertical-align:bottom" src="public/img/accept.png"></img>'+
                           '<select id="select'+ item.id +'"style="width:40px;height:20px;margin-right:5px">'+
                           '<option value="0">0</option>'+
                           '<option value="1">1</option>'+
                           '<option value="2">2</option>'+
                           '<option value="3">3</option>'+
                           '</select>'+
                           '<label id="current_select'+ item.id +'" style="font-size:85%;display:none">[current: '+ (data.configSettings != "" ? data.configSettings.configList[item.id] : item.dv) +']</label>'+
                           '<label id="default_select'+ item.id +'" style="font-size:85%;color:#580f0f">[default: '+ item.dv +']</label>';
          container.appendChild(node);
          var ddl = document.getElementById('select'+ item.id);
          ddl.value = objGeneralConfig ? objGeneralConfig.configList[item.id] : item.dv;
          ddl.disabled = document.getElementById('conf_save_changes_btn') ? false : true;
          ddl.addEventListener("change", function(e){
            var ddl = document.getElementById(e.target.id);
              if(document.getElementById('current_' + e.target.id).innerHTML.indexOf(ddl.options[ddl.selectedIndex].text) > 0){
                document.getElementById('current_' + e.target.id).style.display= 'none';
              }
              else {
                document.getElementById('current_' + e.target.id).style.display= 'inline-block';
              }
          });
        });
      }
    });
  }

  function GetGeneralConfigObject(){
    var GeneralConfigObject = {
      "configList" : "",
      "logicTimeout" : "",
      "delayReset" : "",
      "synchronizationTimeout" : ""
    };
    var configList = {};
    if(dropdownIDList.length > 0){
      dropdownIDList.forEach(function(ddlID){
        configList[ddlID.replace('select','')] = document.getElementById(ddlID).value;
        //configList.push({"id": ddlID.replace('select',""), "value" : document.getElementById(ddlID).value});
      });
      GeneralConfigObject.configList = configList;
      GeneralConfigObject.logicTimeout = document.getElementById('txt_logic_timeout').value;
      GeneralConfigObject.delayReset = document.getElementById('txt_delay_reset').value;
      GeneralConfigObject.synchronizationTimeout = document.getElementById('txt_synchronization_timeout').value;
      console.log(JSON.stringify(GeneralConfigObject));
      return GeneralConfigObject;
    }
    return false;
  }

  function SetCurrentLabel(e, textboxId, currentLabelId, defaultLabelId){
    if(document.getElementById(currentLabelId).innerHTML.indexOf(document.getElementById(textboxId).value) > 0){
      document.getElementById(currentLabelId).style.display = 'none';
    }
    else {
      document.getElementById(currentLabelId).style.display = '';
    }
  }

  function IsValid(oldPassword, newPassword, confirmNewPassword, callback){
    if(oldPassword === newPassword){
      document.getElementById('password_failure').style.display = '';
      document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: New password should not match old one.';
      callback(false);
    }
    else if (newPassword !== confirmNewPassword){
      document.getElementById('password_failure').style.display = '';
      document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: New password and confirm password doesn`t match.';
      callback(false);
    }
    else if(newPassword.length > 12){
      document.getElementById('password_failure').style.display = '';
      document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: New password should contain not more than 12 characters.';
      callback(false);
    }
    else{
      isAccepted(oldPassword, newPassword, callback);
    }
  }

  function isAccepted(currentPass, newPassword, callback){
    socket.emit('validate_current_password', currentPass, newPassword, function(result) {
      if(!result){
        document.getElementById('password_failure').style.display = '';
        document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: Current password is not accepted. Please try again.';
        callback(false);
      }
      else
        callback(true);
    });
  }

  document.observe("dom:loaded", function() {
    BindData();

    $('conf_save_changes_btn').observe('click',function(){
      var objGeneralConfig = GetGeneralConfigObject();
      if(objGeneralConfig){
        SaveToLocalStorage("objGeneralConfig" , objGeneralConfig);
      }
      else{
        ShowAlert("No data to save.");
      }
    });

    $('txt_logic_timeout').observe('change',function(e){
      SetCurrentLabel(e, 'txt_logic_timeout', 'lbl_current_logic_timeout', 'lbl_default_logic_timeout');
    });

    $('txt_synchronization_timeout').observe('change',function(e){
      SetCurrentLabel(e, 'txt_synchronization_timeout', 'lbl_current_synchronization_timeout', 'lbl_default_synchronization_timeout');
    });

    $('conf_restore_Application_btn').observe('click',function(){
      RemoveFromLocalStorage("objGeneralConfig");
      var myNode = document.getElementById('options_list');
      while (myNode.firstChild) {
          myNode.removeChild(myNode.firstChild);
      }
      BindData();
    });

    $('change-pwd').observe('click',function(){
      changePasswordPopup.show({
                   title : 'Change Password',
                   message : ('<div class="field-group">'+
                                '<div class="field-container text clearfix" style="display: none;" id="password_failure">'+
                                ' <label style="color: #AA3333;">FAILURE: Cannot connect to server. Check your network connection and try again.</label>'+
                                '</div>'+
                                '<div class="field-container text clearfix">'+
                                ' <label for="cur_password">Current password:</label>'+
                                ' <div id="cur_password_container" class="iw">'+
                                '   <input id="cur_password" name="cur_password" maxlength="12" type="password">'+
                                ' </div>'+
                                '</div>'+
                                '<div class="field-container text clearfix">'+
                                ' <label for="new_password">New password:</label>'+
                                ' <div id="new_password_container" class="iw">'+
                                '   <input id="new_password" name="new_password" maxlength="12" type="password">'+
                                ' </div>'+
                                '</div>'+
                                '<div class="field-container text clearfix">'+
                                ' <label for="ver_password">Verify new password:</label>'+
                                ' <div id="ver_password_container" class="iw">'+
                                '   <input id="ver_password" name="ver_password" maxlength="12" type="password">'+
                                ' </div>'+
                                '</div>'+
                              '</div>'),
                   width : '370px',
                   buttons : [ {
                               title : 'Cancel',
                               action : function() {
                                          changePasswordPopup.hide();
                                        }
                               },
                               {
                                title : 'Change',
                                action : function(){
                                          submitFunction();
                                         }
                                } ]
                 });

                 $('ver_password').observe('keypress', function(evt) {
                   if (evt.keyCode == 13) {
                     submitFunction();
                   }
                 });
                 $('new_password').observe('keypress', function(evt) {
                   if (evt.keyCode == 13) {
                     submitFunction();
                   }
                 });
                 $('cur_password').observe('keypress', function(evt) {
                   if (evt.keyCode == 13) {
                     submitFunction();
                   }
                 });
    });

    function submitFunction(){
      //validate and perform action.
      document.getElementById('password_failure').style.display = 'none';
      var currentPass = document.getElementById('cur_password').value.trim();
      var newPass = document.getElementById('new_password').value.trim();
      var confirmNewPass = document.getElementById('ver_password').value.trim();
      IsValid(currentPass, newPass, confirmNewPass,function(result){
        if(result){
          SaveToLocalStorage('objNewPassword',{'currentPass' : currentPass, 'newPass' : newPass});
          changePasswordPopup.hide();
        }
      });
    }

  });
})();
