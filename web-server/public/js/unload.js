var save_changes_flag = true;
(function() {
	document.observe("dom:loaded", function() {
		if ( save_changes_flag && $('conf_save_changes_btn')) {
			window.onbeforeunload = function(event) {
				return "Do you want to leave?";
			}
		}
	});
})();
