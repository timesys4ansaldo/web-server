(function() {
	var socket = io();
	socket.emit('get_system_info');

	function getDateString(date) {
		return (date.getMonth()+1) + "/" +
				date.getDate() + "/" +
				date.getFullYear() + " " +
				(date.getHours()<10?"0":"") + date.getHours() + ":" +
				(date.getMinutes()<10?"0":"") + date.getMinutes() + ":" +
				(date.getSeconds()<10?"0":"") + date.getSeconds();
	}

	save_page = "System Information\r\n";
	save_filename = "SysInfo";

	function updatePage(system_info) {
		try {
			save_text = "";
			if (system_info) {
				var appname = system_info.sysinfo.sysInfo.Program;
				if (appname) {
					$('sysinfo-program').innerHTML=appname;
				}
				var execver = system_info.sysinfo.sysInfo.ExecVer;
				if (execver) {
					$('sysinfo-exec-ver').innerHTML = execver;
				}
				var execcrc = system_info.sysinfo.sysInfo.ExecCRC;
				if (execcrc) {
					$('sysinfo-exec-crc').innerHTML = execcrc;
				}
				var compver = system_info.sysinfo.sysInfo.CompVer;
				if (compver) {
					$('sysinfo-comp-ver').innerHTML = compver;
				}
				var appver = system_info.sysinfo.sysInfo.AppVersion;
				if (appver) {
					$('sysinfo-app-ver').innerHTML = appver;
				}
				var appcrc = system_info.sysinfo.sysInfo.AppCRC;
				if (appcrc) {
					$('sysinfo-app-crc').innerHTML = appcrc;
				}
				var siteid = system_info.sysinfo.sysInfo.SiteId;
				if (siteid) {
					$('sysinfo-site-id').innerHTML = siteid;
				}
				var datestamp = system_info.sysinfo.sysInfo.DateStamp;
				if (datestamp) {
					$('sysinfo-date-stamp').innerHTML = datestamp;
				}
				$('sysinfo-maint-ver').innerHTML = $('tool-ver').innerHTML;
				var errortable = $('sys-adjust-tbody-1');
				save_text += "System Adjustment Table:\r\n";
				for(var i = errortable.rows.length - 1; i >= 0; i--){
					errortable.deleteRow(i);
				}
				var errors = system_info.sysinfo.events;
				for (var i=0;i<errors.length;i++) {
					var newrow = errortable.insertRow(i);
					newrow.className = "intable" + (i%2?" alt":"");
					var cell0 = newrow.insertCell(0);
					var cell1 = newrow.insertCell(1);
					var cell2 = newrow.insertCell(2);
					var cell3 = newrow.insertCell(3);
					cell0.innerHTML = errors[i].eventId;
					cell0.className = "sys-adjust-number";
					cell1.innerHTML = errors[i].event;
					cell1.className = "sys-adjust-event";
					cell2.innerHTML = errors[i].DateTime;
					cell2.className = "sys-adjust-time";
					cell3.innerHTML = errors[i].Description;
					cell3.className = "sys-adjust-desc";
					cell3.style.cssText = "width: auto";
					save_text += "Event " + cell0.innerHTML + " <" + cell2.innerHTML + "> " + cell3.innerHTML + "\r\n";
				}
			}
		} catch (h) {
			alert ("Incorrect board data");
			$('sysinfo-program').innerHTML = "";
			$('sysinfo-exec-ver').innerHTML = "";
			$('sysinfo-exec-crc').innerHTML = "";
		  $('sysinfo-comp-ver').innerHTML = "";
		  $('sysinfo-app-ver').innerHTML = "";
		 	$('sysinfo-app-crc').innerHTML = "";
		 	$('sysinfo-site-id').innerHTML = "";
			$('sysinfo-date-stamp').innerHTML = "";
			var errortable = $('sys-adjust-tbody-1');
			for(var i = errortable.rows.length - 1; i >= 0; i--){
				errortable.deleteRow(i);
			}
		}
	}

	document.observe("dom:loaded", function() {
		var boardtime = $('board-time');
		socket.on('system_info', function(system_info) {
				updatePage(system_info);
		});
		boardtime.observe("time:changed", function() {
			$('sysinfo-pc-date').innerHTML = getDateString(new Date());
		});

		if ($('info_clear_btn')) {
			$('info_clear_btn').observe( "click", function() {
				socket.emit('get_system_info');
				socket.on('system_info', function(system_info) {
						updatePage(system_info);
				});
		});
		}

	});
})();
