var checkboxIDList = [];
var last_transport;
var socket = io();

(function() {
  // Wait until dom loded

  function getSelectedCheckboxList(){
    var CheckedItems = [];
    checkboxIDList.forEach(function(id){
      if (document.getElementById(id).checked){
        CheckedItems.push(id.replace("check", ""));
      }
    });
    return CheckedItems;
  }

  function saveToLocalStorage(){
    var CheckedItems = getSelectedCheckboxList();
    var bits = {};
    if(CheckedItems){
      CheckedItems.forEach(function(id){
        bits[id] = "1";
      })
      window.localStorage.setItem('objUserDataLog', JSON.stringify(bits));
    }
  }

  function BindData(){
    socket.emit('get_user_data_logs_list', function(error, data) {
      if(error){
        ShowAlert("Board generated error.");
      }
      else{
        last_transport = data.bitList;
        var checkedItemList = data.selectedBits;
        for(var i =0;i< data.bitList.length;i++){
          checkboxIDList.push('check' + (i+1));
          var container = document.getElementById('options_list');
          var node = document.createElement('div');
          if(window.localStorage['objUserDataLog']){
            checkedItemList = JSON.parse(window.localStorage['objUserDataLog']);
          }
          var checked = checkedItemList != "" ? (parseInt(i + 1) in checkedItemList ? "checked" : "") : "";
          node.id = "div" + (i + 1);
          node.classList.add("user-data-log-dynamic-checkbox-wrapper");
          node.innerHTML = '<input type="checkbox" id="check' + (i + 1) + '" name="check' + data.bitList[i] + '" style="margin-right:5px"' + checked + '><label for="check' + (i+1) + '">'+ data.bitList[i] +'</label>';
          container.appendChild(node);
        }
      }
    });
  }

  function FilterData(){
    var userInput = document.getElementById('start_adr').value.trim().toLowerCase();
    last_transport.map(function (obj) {
      if(obj.toLowerCase().indexOf(userInput) < 0)
          document.getElementById("div" + (last_transport.indexOf(obj) + 1)).style.display = 'none';
      else
          document.getElementById("div" + (last_transport.indexOf(obj) + 1)).style.display = 'block';
    });
  }

  document.observe("dom:loaded", function() {
    BindData();
    $('conf_save_changes_btn').observe('click',function(){
      saveToLocalStorage();
    });

    $('conf_restore_Application_btn').observe('click',function(){
      window.localStorage.removeItem('objUserDataLog');
      document.getElementById('start_adr').value = "";
      var myNode = document.getElementById('options_list');
      while (myNode.firstChild) {
          myNode.removeChild(myNode.firstChild);
      }
      BindData();
    });

    $('search-btn').observe('click',function(){
      FilterData();
    });

  });
})();
