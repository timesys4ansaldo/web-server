// Instance variables.
var messagePopup = new Popup('message');
messagePopup.hide();

// 1. Function to download file.
function DownloadData (dataToBeDownload, fileName, fileType){
  var data = dataToBeDownload;
  var blob = new Blob([data], {type: fileType});
  var url  = URL.createObjectURL(blob);
  if(IsIEorEDGE()){
    if(window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, fileName);
    }
  }
  else {
    var a = document.createElement('a');
    a.download    = fileName;
    a.href        = url;
    var clickEvent = new MouseEvent("click", {
    "view": window,
    "bubbles": true,
    "cancelable": false
    });
    a.dispatchEvent(clickEvent);
  }
}

// 2. Function to download file.
function DownloadCompressedData (dataToBeDownload, fileName, fileType){
  var zip = new JSZip();
  zip.add(fileName, dataToBeDownload);
  content = zip.generate();
  var a = document.createElement('a');
  if(IsIEorEDGE()){
    var binaryString = zip.generate({base64: false}), //By glancing over the source I trust the string is in "binary" form
    len = binaryString.length,    //I.E. having only code points 0 - 255 that represent bytes
    bytes = new Uint8Array(len);
    for( var i = 0; i < len; ++i ) {
        bytes[i] = binaryString.charCodeAt(i);
    }
    var file = new Blob([bytes], {type:fileName});
    if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(file, fileName.split('.')[0] + ".zip");
    }
  }
  else {
    a.href = "data:" + fileType + ";base64," + content;
    a.download = fileName.split('.')[0] + ".zip";
    var clickEvent = new MouseEvent("click", {
      "view": window,
      "bubbles": true,
      "cancelable": false
    });
    a.dispatchEvent(clickEvent);
  }
}

// 3. Function to detect Microsoft family browsers.
function IsIEorEDGE(){
  if (/MSIE 10/i.test(navigator.userAgent)) {
     // This is internet explorer 10
     return true;
  }

  if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
      // This is internet explorer 9 or 11
      return true;
  }

  if (/Edge\/\d./i.test(navigator.userAgent)){
     // This is Microsoft Edge
     return true;
  }
  return false;
}

// 4. Function to show alert message.
function ShowAlert(alertMessage){
 messagePopup.show({
              title : 'Message',
              message : ('<p style="text-align:center">' + alertMessage + '</p>'),
              width : '370px'
            });
}

// 5. Function to save json object in local storage.
function SaveToLocalStorage(key, obj){
  window.localStorage.setItem(key, JSON.stringify(obj));
}

// 6. Function to get object from local storage.
function GetFromLocalStorage(key){
  if(window.localStorage[key])
    return JSON.parse(window.localStorage[key]);
  else return false;
}

// 6. Function to remove object from local storage.
function RemoveFromLocalStorage(obj){
  window.localStorage.removeItem(obj);
}

// 7. Function to clear local storage
function ClearLocalStorage(){
  window.localStorage.clear();
}
