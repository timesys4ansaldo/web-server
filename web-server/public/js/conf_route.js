(function() {
	var socket = io();
	socket.emit('router');

	function updateRoutingTable(router_info) {
				try {
					if ( router_info ) {
						var status = router_info.status;
						if (status == 0) {
							/*Clear field*/
							$('route-log-field').value = "";
							var routes = router_info.route;
							for (var i=0;i<routes.length;i++) {
								/*Add route to field*/
								$("route-log-field").value += routes[i];
							}
						} else {
							ShowAlert("Wrong data received. Refresh page to retry.");
						}
					} else {
						ShowAlert("Wrong data received. Refresh page to retry.");
					}
				} catch (h) {
					ShowAlert("Incorrect data received. Refresh page to retry.");
				}
	}

	document.observe("dom:loaded", function() {
		socket.on('router_info', function(router_info) {
				updateRoutingTable(router_info);
		});
		//ajax_overlay.show();
		//$('route_submit_btn').observe('click', executeCommand);
		$('route_submit_btn').observe('click', function(){
			var cmdin = document.getElementById('route-command').value.toString();
			socket.emit('cmd', {message : cmdin});
		});

		socket.on('err_cmd', function(){
			$('route-log-field').value = "";
			ShowAlert("Command executing error: Invalid Command");
		});
	});
})();
