var timerIDList = [];
var last_transport;
var socket = io();

(function() {
  // Wait until dom loded


  function BindData(){

    socket.emit('get_timer_config_list', function(error, data) {
      if(error){
        ShowAlert("No data found for General Configuration.");
      }
      else{
          last_transport = data.ConfigList;
          var objTimerConfig = GetFromLocalStorage('objTimerConfig') ? GetFromLocalStorage('objTimerConfig') : (data.configSettings != "" ? data.configSettings : false);
          data.configData.timerConfigList.forEach(function(item){
          timerIDList.push(item.id);
          var container = document.getElementById('TimerList');
          var node = document.createElement('div');
          node.id = "div" + item.id;
          node.classList.add("field-group");
          node.classList.add("clearfix");
          node.setAttribute("style", "margin: 0 0 10px 0");
          node.innerHTML = '<h2>'+item.id+'</h2>'+
                            '<div class="field-container text clearfix">'+
                            ' <label for="SetDelay0" style="width: 200px; float: left; margin-top: 5px;">Set Delay:</label>'+
                            ' <img id="img_SetDelay0" style="float: left; display: block;" src="public/img/Warning.png">'+
                            ' <div id="DivSetDelay' + item.id + '" class="iw suffix" style="width: 100px; float: left; margin: 0 5px;">'+
                            '   <input id="SetDelay' + item.id + '" style="height:15px;background-color:transprent" class="cfgproc" step="1" min="0" max="6553500" maxlength="10" value="' + (objTimerConfig ? objTimerConfig[item.id].setDelaySV : item.setDelayDV) + '" name="SetDelay0" type="number">'+
                            '   <span>milliseconds</span>'+
                            ' </div>'+
                            ' <div class="validation-error" style="width: 100px; float: left; margin-top: 5px;">[default: <span id="def_SetDelay0">' + item.setDelayDV + '</span>]'+
                            ' </div>'+
                            '</div>'+
                            '<div class="field-container text clearfix">'+
                            '  <label for="ClearDelay0" style="width: 200px; float: left; margin-top: 5px;">Clear Delay:</label>'+
                            '  <img id="img_ClearDelay0" style="float: left; display: block;" src="public/img/Warning.png">'+
                            '  <div id="DivClearDelay' + item.id + '" class="iw suffix" style="width: 100px; float: left; margin: 0 5px;">'+
                            '    <input id="ClearDelay' + item.id + '" style="height:15px;background-color:transprent" class="cfgproc" step="1" min="0" max="6553500" maxlength="10" value="' + (objTimerConfig ? objTimerConfig[item.id].clearDelaySV : item.clearDelayDV) + '" name="ClearDelay0" type="number">'+
                            '    <span>milliseconds</span>'+
                            '  </div>'+
                            ' <div class="validation-error" style="width: 100px; float: left; margin-top: 5px;">[default: <span id="def_ClearDelay0">' + item.clearDelayDV + '</span>]'+
                            ' </div>'+
                            '</div>';
          container.appendChild(node);
          document.getElementById('SetDelay' + item.id).disabled = document.getElementById('conf_save_changes_btn') ? false : true;
          if(document.getElementById('SetDelay' + item.id).disabled){
            document.getElementById('DivSetDelay' + item.id).classList.add('is-disabled');
          }
          document.getElementById('ClearDelay' + item.id).disabled = document.getElementById('conf_save_changes_btn') ? false : true;
          if(document.getElementById('ClearDelay' + item.id).disabled){
            document.getElementById('DivClearDelay' + item.id).classList.add('is-disabled');
          }
          //var ddl = document.getElementById('select'+ item.id);
          //ddl.value = objGeneralConfig ? objGeneralConfig.configList[item.id] : item.dv;
          //ddl.disabled = document.getElementById('conf_save_changes_btn') ? false : true;
          //ddl.addEventListener("change", function(e){
            //var ddl = document.getElementById(e.target.id);
              //if(document.getElementById('default_' + e.target.id).innerHTML.indexOf(ddl.options[ddl.selectedIndex].text) > 0){
                //document.getElementById('current_' + e.target.id).style.display= 'none';
              //}
              //else {
                //document.getElementById('current_' + e.target.id).style.display= 'inline-block';
              //}
          //});
        });
      }
    });
  }

  function GetTimerConfigObject(){
    var configList = {};
    if(timerIDList.length > 0){
      timerIDList.forEach(function(ID){
        configList[ID] = { "setDelaySV" : document.getElementById('SetDelay' + ID).value, "clearDelaySV" : document.getElementById('ClearDelay' + ID).value};
      });
      return configList;
    }
    return false;
  }

  function SetCurrentLabel(e, textboxId, currentLabelId, defaultLabelId){
    if(document.getElementById(defaultLabelId).innerHTML.indexOf(document.getElementById(textboxId).value) > 0){
      document.getElementById(currentLabelId).style.display = 'none';
    }
    else {
      document.getElementById(currentLabelId).style.display = '';
    }
  }

  document.observe("dom:loaded", function() {
    BindData();

    $('conf_save_changes_btn').observe('click',function(){
      var objTimerConfig = GetTimerConfigObject();
      if(objTimerConfig){
        console.log(objTimerConfig);
        SaveToLocalStorage("objTimerConfig" , objTimerConfig);
      }
      else{
        ShowAlert("No data to save.");
      }
    });


    $('conf_restore_Application_btn').observe('click',function(){
      RemoveFromLocalStorage("objTimerConfig");
      var myNode = document.getElementById('TimerList');
      while (myNode.firstChild) {
          myNode.removeChild(myNode.firstChild);
      }
      BindData();
    });

  });
})();
