var socket = io();
var messagePopup = new Popup('message');
(function() {

    function ExitConfigMode(){
      socket.emit('expire-logged-in-session', function(response){
        if(response.status == "1"){
          window.location.href = "/";
        }
      });
    }

    document.observe("dom:loaded", function() {
      $('apply_changes_btn').observe('click', function(){
        var dataToSave = {};
        if(window.localStorage.length > 0) {
          var keyList = Object.keys(window.localStorage);
          keyList.forEach(function(key){
            dataToSave[key] = JSON.parse(window.localStorage[key]);
          });
          if(dataToSave.hasOwnProperty('objNewPassword')){
            var objNewPassword = dataToSave['objNewPassword'];
            delete dataToSave['objNewPassword'];
            socket.emit('change_password', objNewPassword['currentPass'], objNewPassword['newPass'], function(error) {
              if(error)
                console.log(error);
            });
          }
          socket.emit('save_local_storage_data', JSON.stringify(dataToSave) , function(error) {
            if(error){
              console.log("error");
            }
            else {

              ClearLocalStorage();
              ExitConfigMode();
            }
          });
        }
        else {
          messagePopup.show({
                       title : 'Message',
                       message : ('<p style="text-align:center"> No local changes to upload. </p>'),
                       width : '370px',
                       buttons : [ {
                             title : 'OK',
                             action : function() {
                                        ExitConfigMode();
                                      }
                                  } ]
                      });
        }
      });

      $('discard_changes_btn').observe('click', function(){
        ClearLocalStorage();
        ExitConfigMode();
      });

    });
})();
