var socket = io();
var statusPopup = new Popup('status-popup');
statusPopup.hide();

(function(){
  // wait until DOM loaded.
  document.observe("dom:loaded", function(){
    var edit_mode = location.search.split('edit_mode=')[1];
    if (!$('edit_mode')){
      statusPopup.show({
            title : 'Authenticated User Error',
            message : '<p style="text-align:center">' + 'This feature is only available to Authenticated User. Please login to Clear Configuration.' + '</p>',
            buttons : [ {
              title : 'Dismiss',
              action : function() {
                window.location.href = '/config';
                window.onbeforeunload = null;
              }
            } ]
          });
    return;
    }
    // clear configuration button click event.
    $('clear-sysconfig').observe('click', function(){

      socket.emit('clear_configurations', function(error) {
        if(error){
          if(error.indexOf('no such file or directory') > 0){
            ShowAlert("No saved configuration to clear");
          }
          else{
            ShowAlert("Error while clearing configuration.");
          }
        }
        else{
          ShowAlert("Configuration cleared successfully.")
        }
      });

    });
  });
})();
