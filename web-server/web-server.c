#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[])
{
    pid_t cpid;
    int   status;
    cpid = fork();

    char *cmd[10] = { 0 };
    char cmd_string[10][20] = { { 0 } };

    sprintf(cmd_string[0], "/usr/bin/node");
    sprintf(cmd_string[1], "server.js");
    cmd[0] = cmd_string[0];
    cmd[1] = cmd_string[1];
    cmd[2] = NULL;

	for (;; )
	{
        switch (cpid)
        {
        case -1:  // Fork failure
            exit(EXIT_FAILURE);
            break;

        case 0:  // Child Process
            execv(cmd[0], cmd);
            return 0; // exit child process
            break;

        default:  // Parent process
            if (waitpid(-1, &status, WUNTRACED) != 1)
                cpid = fork();                                     // restart
            // Do parent stuff here...
            break;
        }
		sleep(2);
    }
}
