SUMMARY = "This is the web server."
DESCRIPTION = "This installs the web server."
SECTION = "web-server"
LICENSE = "CLOSED"

DEPENDS = "sqlite3"
RDEPENDS_${PN} = "\
    nodejs \
    bash \
    sqlite3"

SRC_URI = " \
           file://package.json \
           file://config.js \
           file://readme \
           file://server.js \
           file://web-server.c \
           file://start.js \
           file://socket_events.js \
           file://lib/login.js \
           file://lib/pass.js \
           file://public/css/jquery-ui.css \
           file://public/css/jquery-ui-timepicker-addon.css \
           file://public/css/style.css \
           file://public/css/images/animated-overlay.gif \
           file://public/css/images/ui-bg_flat_0_aaaaaa_40x100.png \
           file://public/css/images/ui-bg_flat_75_ffffff_40x100.png \
           file://public/css/images/ui-bg_glass_55_fbf9ee_1x400.png \
           file://public/css/images/ui-bg_glass_65_ffffff_1x400.png \
           file://public/css/images/ui-bg_glass_75_dadada_1x400.png \
           file://public/css/images/ui-bg_glass_75_e6e6e6_1x400.png \
           file://public/css/images/ui-bg_glass_95_fef1ec_1x400.png \
           file://public/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png \
           file://public/css/images/ui-icons_222222_256x240.png \
           file://public/css/images/ui-icons_2e83ff_256x240.png \
           file://public/css/images/ui-icons_454545_256x240.png \
           file://public/css/images/ui-icons_888888_256x240.png \
           file://public/css/images/ui-icons_cd0a0a_256x240.png \
           file://public/img/accept.png \
           file://public/img/ajax-load.gif \
           file://public/img/Ansaldo_STS_Htachi_neg.jpg \
           file://public/img/arrowinfo.png \
           file://public/img/arrow.png \
           file://public/img/arrowup.png \
           file://public/img/bg.png \
           file://public/img/error.png \
           file://public/img/exclamation.png \
           file://public/img/information.png \
           file://public/img/lightbg.gif \
           file://public/img/subsection-bg.png \
           file://public/img/top_bg.gif \
           file://public/img/top_bg.png \
           file://public/img/Warning.png \
	   file://public/img/warning.png \
           file://public/img/bitandvars/highlevel_go.png \
           file://public/img/bitandvars/highlevel_off.png \
           file://public/img/bitandvars/highlevel_on.png \
           file://public/img/bitandvars/highlevel_up.png \
           file://public/img/bitandvars/lowlevel_down.png \
           file://public/img/bitandvars/lowlevel_go.png \
           file://public/img/bitandvars/lowlevel_off.png \
           file://public/img/bitandvars/lowlevel_on.png \
           file://public/img/bitandvars/midlevel_go.png \
           file://public/img/bitandvars/midlevel_off.png \
           file://public/img/bitandvars/midlevel_on.png \
           file://public/img/bitandvars/signal_off.png \
           file://public/img/bitandvars/timeline.png \
           file://public/js/bitandvars.js \ 
           file://public/js/common_function.js \
	   file://public/js/conf_clear.js \
	   file://public/js/conf_download.js \
	   file://public/js/conf_general.js \
           file://public/js/conf_route.js \
	   file://public/js/conf_timers.js \
   	   file://public/js/conf_upload.js \
	   file://public/js/conf_userdatalog.js \
	   file://public/js/config.js \
	   file://public/js/errorlog.js \
	   file://public/js/eventlog.js \
	   file://public/js/filexhr.js \
	   file://public/js/header_info.js \
	   file://public/js/init.js \
	   file://public/js/popup.js \
	   file://public/js/session.js \
	   file://public/js/sessionlimit.js \
           file://public/js/socket.io-stream.js \
	   file://public/js/sysinfo.js \
 	   file://public/js/unload.js \
	   file://public/js/lib/dd_belatedpng_0.0.8a-min.js \
           file://public/js/lib/effects.js \	   
           file://public/js/lib/ie6-png-fix.js \
           file://public/js/lib/jqnoconflict.js \
           file://public/js/lib/jquery-1.11.1.min.js \
	   file://public/js/lib/jquery-ui-sliderAccess.js \
	   file://public/js/lib/jquery-ui-timepicker-addon-i18n.min.js \
	   file://public/js/lib/jquery-ui-timepicker-addon.js \
	   file://public/js/lib/jquery-ui.min.js \
           file://public/js/lib/jquery.min.js \
           file://public/js/lib/jszip.js \
           file://public/js/lib/modernizr-1.7.min.js \
           file://public/js/lib/prototype.js \
	   file://routes/routes.js \
	   file://utils/logger.js \
	   file://utils/db_operations.js \          
	   file://views/bitandvars.ejs \
	   file://views/block.html \
           file://views/conf_boards.ejs \
           file://views/conf_clear.ejs \
           file://views/conf_download.ejs \
           file://views/conf_general.ejs \
           file://views/conf_header.ejs \
           file://views/conf_links.ejs \
           file://views/conf_route.ejs \
	   file://views/conf_snmp.ejs \
           file://views/conf_timers.ejs \
           file://views/conf_upload.ejs \
           file://views/conf_userdatalog.ejs \
           file://views/conf_variables.ejs \
	   file://views/config.ejs \
           file://views/errorlog.ejs \
           file://views/eventlog.ejs \
           file://views/footer.ejs \
           file://views/header.ejs \
           file://views/index.ejs \
           file://views/linkinfo.ejs \
           file://views/logicmon.ejs \
           file://views/memorydump.ejs \
           file://views/messagemon.ejs \
           file://views/sysinfo.ejs \
           file://views/systime.ejs \
           file://views/userdatalog.ejs \
           file://node_modules.xx \
      	   file://database \
      	   file://database/microlok_eventlog_database.db \
      	   file://system_bash/network_routing.sh \
           file://init_script/web-server \
	   file://static_data_json/bitstore.json \
	   file://static_data_json/general_config_list.json \
           file://static_data_json/groupbits.json \
      	   file://static_data_json/system_information.json \
	   file://static_data_json/timer_config_list.json \
      	   file://static_data_json/users.json \
      	   file://certificates/key.pem	\
      	   file://certificates/cert.pem \
           "
S = "${WORKDIR}"

inherit update-rc.d
INITSCRIPT_NAME = "web-server"
INITSCRIPT_PARAMS = "defaults 69"

do_compile () {
    ${CC} web-server.c -o web-server
}

do_install () {
    INSTDIR="/home/www/";
    #create directories
    install -d ${D}${INSTDIR}
    install -d ${D}${INSTDIR}/public/css
    install -d ${D}${INSTDIR}/public/css/images
    install -d ${D}${INSTDIR}/public/img
    install -d ${D}${INSTDIR}/public/img/bitandvars
    install -d ${D}${INSTDIR}/public/js
    install -d ${D}${INSTDIR}/public/js/lib
    install -d ${D}${INSTDIR}/routes
    install -d ${D}${INSTDIR}/lib
    install -d ${D}${INSTDIR}/utils
    install -d ${D}${INSTDIR}/views
    install -d ${D}${INSTDIR}/system_bash
    install -d ${D}${INSTDIR}/static_data_json
    install -d ${D}${INSTDIR}/certificates
    install -d ${D}${INSTDIR}/database
    install -d ${D}${sysconfdir}/init.d
    install -d ${D}${bindir}

    #install files
    install -m 755 ${WORKDIR}/config.js                 ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/node_modules.xx		${D}${INSTDIR}
    install -m 755 ${WORKDIR}/package.json              ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/readme                    ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/server.js                 ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/socket_events.js          ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/start.js                  ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/web-server.c              ${D}${bindir}

    #install lib
    install -m 755 ${WORKDIR}/lib/login.js       ${D}${INSTDIR}/lib/
    install -m 755 ${WORKDIR}/lib/pass.js        ${D}${INSTDIR}/lib/

    #install css
    install -m 755 ${WORKDIR}/public/css/jquery-ui.css                                     ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/jquery-ui-timepicker-addon.css                    ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/style.css                                         ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/images/animated-overlay.gif                       ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_flat_0_aaaaaa_40x100.png             ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_flat_75_ffffff_40x100.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_55_fbf9ee_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_65_ffffff_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_75_dadada_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_75_e6e6e6_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_95_fef1ec_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png   ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_222222_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_2e83ff_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_454545_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_888888_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_cd0a0a_256x240.png                ${D}${INSTDIR}/public/css/images/

    #install img files
    install -m 755 ${WORKDIR}/public/img/accept.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/ajax-load.gif                  ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/Ansaldo_STS_Htachi_neg.jpg     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/arrowinfo.png                  ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/arrow.png                      ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/arrowup.png                    ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/bg.png                         ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/error.png                      ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/exclamation.png                ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/information.png                ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/lightbg.gif                    ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/subsection-bg.png              ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/top_bg.gif                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/top_bg.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/Warning.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/warning.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_go.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_off.png   ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_on.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_up.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_down.png   ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_go.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_off.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_on.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/midlevel_go.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/midlevel_off.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/midlevel_on.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/signal_off.png      ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/timeline.png        ${D}${INSTDIR}/public/img/bitandvars/

    #install js files

    install -m 755 ${WORKDIR}/public/js/bitandvars.js                                ${D}${INSTDIR}/public/js/    
    install -m 755 ${WORKDIR}/public/js/common_function.js                           ${D}${INSTDIR}/public/js/    
    install -m 755 ${WORKDIR}/public/js/conf_clear.js                                ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_download.js                             ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_general.js                              ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_route.js                                ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_timers.js                               ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_upload.js                               ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_userdatalog.js                          ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/config.js                          	     ${D}${INSTDIR}/public/js/	
    install -m 755 ${WORKDIR}/public/js/errorlog.js                          	     ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/eventlog.js                          	     ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/filexhr.js                                   ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/header_info.js                               ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/init.js                                      ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/popup.js                                     ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/session.js                                   ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/sessionlimit.js                              ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/socket.io-stream.js                          ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/sysinfo.js                                   ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/unload.js                                    ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/lib/dd_belatedpng_0.0.8a-min.js              ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/effects.js                               ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/ie6-png-fix.js                           ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jqnoconflict.js                          ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-1.11.1.min.js                     ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui-sliderAccess.js                ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui-timepicker-addon-i18n.min.js   ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui-timepicker-addon.js            ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui.min.js                         ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery.min.js                            ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jszip.js                                 ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/modernizr-1.7.min.js                     ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/prototype.js                             ${D}${INSTDIR}/public/js/lib/

    #install routes
    install -m 755 ${WORKDIR}/routes/routes.js ${D}${INSTDIR}/routes/

    #install utils
    install -m 755 ${WORKDIR}/utils/logger.js ${D}${INSTDIR}/utils/
    install -m 755 ${WORKDIR}/utils/db_operations.js ${D}${INSTDIR}/utils/

    #install certificates
    install -m 755 ${WORKDIR}/certificates/key.pem ${D}${INSTDIR}/certificates/
    install -m 755 ${WORKDIR}/certificates/cert.pem ${D}${INSTDIR}/certificates/

    #install static_data_json

    install -m 755 ${WORKDIR}/static_data_json/bitstore.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/general_config_list.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/groupbits.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/system_information.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/timer_config_list.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/users.json ${D}${INSTDIR}/static_data_json/

    #install system_bash
    install -m 755 ${WORKDIR}/system_bash/network_routing.sh ${D}${INSTDIR}/system_bash/

    #install views
    install -m 755 ${WORKDIR}/views/bitandvars.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/block.html             ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_boards.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_clear.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_download.ejs      ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_general.ejs       ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_header.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_links.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_route.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_snmp.ejs          ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_timers.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_upload.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_userdatalog.ejs   ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_variables.ejs     ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/config.ejs             ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/errorlog.ejs           ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/eventlog.ejs           ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/footer.ejs             ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/header.ejs             ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/index.ejs              ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/linkinfo.ejs           ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/logicmon.ejs           ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/memorydump.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/messagemon.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/sysinfo.ejs            ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/systime.ejs            ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/userdatalog.ejs        ${D}${INSTDIR}/views/


    #install database modules
    install -m 755 ${WORKDIR}/database/microlok_eventlog_database.db ${D}${INSTDIR}/database/

    #install node_modules files
    install -m 755 ${WORKDIR}/node_modules.xx ${D}${INSTDIR}


    #install startup script
    install -m 755 ${WORKDIR}/init_script/web-server ${D}${sysconfdir}/init.d

}
FILES_${PN} = "/"

PACKAGE_ARCH = "${MACHINE_ARCH}"
